<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmMedicalVisitorServiceRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('service_requests', function (Blueprint $table) {
            $table->integer('medical_visitor_id')->unsigned()->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('service_requests', function (Blueprint $table) {
            $table->dropColumn('medical_visitor_id');
        });
    }
}
