<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('administrative_ID', 45)->index();
            $table->string('api_token', 60)->index();
            /*$table->string('administrative_ID', 45)->unique()->index(); DESCOMENTEN ESTAS DOS LINEAS Y COMENTEN LAS DOS DE ARRIBA PARA EL AMBIENTE EN PRODUCCION SIN APIS REPETIDAS
            $table->string('api_token', 60)->unique()->index();*/
            $table->rememberToken();
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
