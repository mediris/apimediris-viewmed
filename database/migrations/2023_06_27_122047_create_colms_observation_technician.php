<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmsObservationTechnician extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requested_procedures', function (Blueprint $table) {

            $table->text('observation_technician');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requested_procedures', function (Blueprint $table) {

            $table->dropColumn('observation_technician');

        });
    }
}
