<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLogitudReceptorNameToDelivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up(){
        Schema::table('delivers', function (Blueprint $table) {
            $table->string('receptor_name', 255)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('delivers', function (Blueprint $table) {
            $table->string('receptor_name', 10)->change();
        });
    }
}
