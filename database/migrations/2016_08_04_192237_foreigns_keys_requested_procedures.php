<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysRequestedProcedures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requested_procedures', function (Blueprint $table) {

            $table->foreign('service_request_id')->references('id')->on('service_requests')->onDelete('restrict');
            $table->foreign('procedure_id')->references('id')->on('procedures')->onDelete('restrict');
            $table->foreign('requested_procedure_status_id')->references('id')->on('requested_procedure_statuses')->onDelete('restrict');
            $table->foreign('plates_size_id')->references('id')->on('plates_sizes')->onDelete('restrict');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('restrict');
            $table->foreign('sub_category_id')->references('id')->on('sub_categories')->onDelete('restrict');
            $table->foreign('bi_rad_id')->references('id')->on('bi_rads')->onDelete('restrict');
            $table->foreign('suspension_reason_id')->references('id')->on('suspend_reasons');
            $table->foreign('equipment_id')->references('id')->on('equipment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requested_procedures', function (Blueprint $table) {
            
            $table->dropForeign('requested_procedures_service_request_id_foreign');
            $table->dropForeign('requested_procedures_procedure_id_foreign');
            $table->dropForeign('requested_procedures_requested_procedure_status_id_foreign');
            $table->dropForeign('requested_procedures_plates_size_id_foreign');
            $table->dropForeign('requested_procedures_category_id_foreign');
            $table->dropForeign('requested_procedures_sub_category_id_foreign');
            $table->dropForeign('requested_procedures_bi_rad_id_foreign');
            $table->dropForeign('requested_procedures_suspension_reason_id_foreign');
        });
    }
}
