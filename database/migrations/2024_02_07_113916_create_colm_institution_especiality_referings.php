<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmInstitutionEspecialityReferings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::table('referrings', function (Blueprint $table) {
            $table->string('referring_institution', 250);
            $table->string('speciality', 250);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
       Schema::table('referrings', function (Blueprint $table) {
            $table->dropColumn('referring_institution');
            $table->dropColumn('speciality');
        });
    }
}
