<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmInstitutionIdSearchIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
        public function up(){

        $this->dropView();
        $this->createView();
    }

    public function createView(){

        DB::statement("

        CREATE VIEW searchIndexView AS
         SELECT
            pt.icon AS patientTypeIcon,
            pt.description AS patientType,
            rps.description AS orderStatus,
            rp.id AS orderID,
            rps.id AS orderStatusID,
            rp.study_instance_id AS orderIdentificationID,
            concat(sr.patient_first_name,
            ' ', sr.patient_last_name) AS patientName,
            sr.patient_identification_id AS patientIdentificationID,
            sr.patient_id AS patientID,
            p.description AS procedureDescription,
            (CASE
                WHEN (r.first_name IS NOT NULL) THEN CONCAT(r.first_name, ' ', r.last_name)
                ELSE 'N/A'
            END) AS referring,
            rp.approve_user_name AS approveUserName,
            rp.text AS reportText,
            /*ad.text AS addendumText,*/
            rp.radiologist_user_name AS radiologistUserName,
            rp.radiologist_user_id AS radiologistUserID,
            sr.issue_date AS admissionDate,
            rp.technician_end_date AS technicianEndDate,
            rp.dictation_date AS dictationDate,
            rp.transcription_date AS transcriptionDate,
            rp.approval_date AS approvalDate,
            rp.suspension_date AS suspensionDate,
            br.id AS biradID,
            sr.id AS serviceRequestID,
            sr.institution_id AS institutionId,
            (CASE sr.patient_sex_id
            WHEN 1 THEN 'male'
            WHEN 2 THEN 'female'
            ELSE '' END) AS patientSex,
            sr.patient_cellphone AS cellPhone,
            m.name AS modalities,
            rp.teaching_file_text AS teachingFileText,
            rp.category_id AS categoryID,
            rp.sub_category_id AS subCategoryID,
            rp.biopsy_result AS biopsyResult
        FROM requested_procedures rp
            JOIN service_requests sr ON rp.service_request_id = sr.id
            JOIN procedures p ON rp.procedure_id = p.id
            JOIN requested_procedure_statuses rps ON rps.id = rp.requested_procedure_status_id
            JOIN patient_types pt ON sr.patient_type_id = pt.id
            /* JOIN (select ad1.id AS id,
            ad1.text AS text,
            ad1.requested_procedure_id AS requested_procedure_id
            FROM addendums ad1
            where (select count(0)
            FROM addendums ad2
            where ad2.requested_procedure_id = ad1.requested_procedure_id
            and ad2.id > ad1.id) = 0) ad ON ad.requested_procedure_id = rp.id */
            JOIN modalities m ON m.id = p.modality_id
            LEFT JOIN referrings r ON sr.referring_id = r.id
            LEFT JOIN bi_rads br ON rp.bi_rad_id = br.id
        ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
        public function dropView(){

        DB::statement('DROP VIEW IF EXISTS searchIndexView');
    }
     public function down(){
        
        $this->dropView();
    }
}
