<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewForeingsKeysServiceRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('service_requests', function (Blueprint $table) {
            $table->foreign('medical_visitor_id')->references('id')->on('medical_visitors')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('service_requests', function (Blueprint $table) {
            $table->dropForeign('service_requests_medical_visitor_id_foreign');
        });
    }
}
