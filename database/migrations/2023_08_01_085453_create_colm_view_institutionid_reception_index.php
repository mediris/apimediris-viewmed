<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmViewInstitutionidReceptionIndex extends Migration{ 
/**
         * Run the migrations.
         *
         * @return void
         */

        public function up()
    {
        $this->dropView();
        $this->createView();
    }
        public function createView () {

            DB::statement("
            CREATE VIEW `receptionIndexView` AS
                SELECT
                    `a`.`id` AS `id`,
                    `a`.`patient_id` AS `patientID`,
                    `a`.`patient_identification_id` AS `patientIdentificationID`,
                    `a`.`patient_last_name` AS `lastName`,
                    `a`.`patient_first_name` AS `firstName`,
                    `p`.`description` AS `procedureName`,
                    `r`.`description` AS `room`,
                    `a`.`appointment_date_start` AS `date`,
                    `a`.`appointment_date_start` AS `dateStartTime`,
                    `a`.`appointment_date_end` AS `dateEndTime`,
                    `a`.`institution_id` AS `institutionId`,
                    `aps`.`description` AS `statusDescription`,
                    `aps`.`status_color` AS `statusColor`
                FROM
                    (((`appointments` `a`
                    JOIN `procedures` `p` ON ((`p`.`id` = `a`.`procedure_id`)))
                    JOIN `rooms` `r` ON ((`r`.`id` = `a`.`room_id`)))
                    JOIN `appointment_statuses` `aps` ON ((`aps`.`id` = `a`.`appointment_status_id`)))
                WHERE `aps`.`description` = 'created'
        ");
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */


        public function dropView () {

            DB::statement("DROP VIEW IF EXISTS receptionIndexView");
        }
        
        public function down(){

            $this->dropView();
        }
    }
