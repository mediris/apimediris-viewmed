<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmDocumentTypeViewRadiologistAddendum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->dropView();
        $this->createView();
    }

    public function createView(){

        DB::statement('
        
            CREATE VIEW `apimeditron`.`radiologist_addendum` AS
        SELECT 
        `apimeditron`.`requested_procedures`.`id` AS `orderID`,
        `apimeditron`.`requested_procedures`.`service_request_id` AS `serviceRequestID`,
        `apimeditron`.`requested_procedures`.`blocked_status` AS `blockedStatus`,
        `apimeditron`.`requested_procedures`.`blocking_user_id` AS `blockingUserID`,
        `apimeditron`.`requested_procedures`.`blocking_user_name` AS `blockingUserName`,
        `apimeditron`.`requested_procedures`.`technician_user_name` AS `technicianUserName`,
        `apimeditron`.`requested_procedures`.`radiologist_user_name` AS `radiologistUserName`,
        `apimeditron`.`requested_procedures`.`transcriptor_user_name` AS `transcriptorUserName`,
        `apimeditron`.`requested_procedures`.`approve_user_name` AS `approveUserName`,
        `apimeditron`.`requested_procedures`.`approval_date` AS `approvalDate`,
        `apimeditron`.`requested_procedure_statuses`.`description` AS `orderStatus`,
        `apimeditron`.`requested_procedure_statuses`.`id` AS `orderStatusID`,
        `apimeditron`.`procedures`.`description` AS `procedureDescription`,
        `apimeditron`.`patient_types`.`description` AS `patientType`,
        `apimeditron`.`patient_types`.`icon` AS `patientTypeIcon`,
        `apimeditron`.`service_requests`.`patient_first_name` AS `patientFirstName`,
        `apimeditron`.`service_requests`.`patient_last_name` AS `patientLastName`,
        (CASE
            WHEN `patients`.`document_type` IS NULL OR `patients`.`document_type` = \' \' THEN `service_requests`.`patient_identification_id`
            ELSE CONCAT(`patients`.`document_type`, \' - \', `service_requests`.`patient_identification_id`)
        END) AS `patientID`,
        `apimeditron`.`patient_types`.`color` AS `patientTypeColor`,
        `apimeditron`.`service_requests`.`institution_id` AS `institutionId`,
        `apimeditron`.`requested_procedures`.`urgent` AS `urgent`
        FROM
            (((((`apimeditron`.`requested_procedures`
            JOIN `apimeditron`.`service_requests` ON ((`apimeditron`.`requested_procedures`.`service_request_id` = `apimeditron`.`service_requests`.`id`)))
            JOIN `apimeditron`.`procedures` ON ((`apimeditron`.`requested_procedures`.`procedure_id` = `apimeditron`.`procedures`.`id`)))
            JOIN `mediris`.`patients` ON ((`service_requests`.`patient_identification_id` = `patients`.`patient_ID`)))
            JOIN `apimeditron`.`requested_procedure_statuses` ON ((`apimeditron`.`requested_procedure_statuses`.`id` = `apimeditron`.`requested_procedures`.`requested_procedure_status_id`)))
            JOIN `apimeditron`.`patient_types` ON ((`apimeditron`.`service_requests`.`patient_type_id` = `apimeditron`.`patient_types`.`id`)))
        WHERE
            (`apimeditron`.`requested_procedure_statuses`.`id` = 6)
       
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function dropView(){

        DB::statement('DROP VIEW IF EXISTS radiologist_addendum');
    }

    public function down()
    {
        $this->dropView();
    }
}
