<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_requests', function (Blueprint $table) {

            $table->increments('id');
            $table->boolean('active');
            $table->boolean('anonymous');
            $table->string('patient_first_name', 70)->index();
            $table->string('patient_last_name', 70)->index();
            $table->string('patient_identification_id', 45)->index();
            $table->string('patient_cellphone', 45)->index();
            $table->integer('patient_id')->unsigned()->index();
            $table->integer('patient_sex_id')->unsigned()->index();
            $table->integer('request_status_id')->unsigned()->index();
            $table->integer('patient_type_id')->unsigned()->index();
            $table->integer('source_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('referring_id')->unsigned()->index()->nullable();
            $table->integer('answer_id')->unsigned()->index();
            $table->float('weight');
            $table->float('height');
            $table->string('parent', 60);
            $table->integer('pregnancy_status_id')->unsigned()->index()
                ->nullable();
            $table->integer('smoking_status_id')->unsigned()->index();
            $table->dateTime('issue_date');
            $table->integer('patient_state_id')->unsigned()->index();
            $table->dateTime('last_menstrual_date');
            $table->text('comments');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_requests');
    }
}
