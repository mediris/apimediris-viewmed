<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysConfigurations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configurations', function($table){

            $table->foreign('appointment_sms_template_id')->references('id')->on('notification_templates')->onDelete('cascade');
            $table->foreign('appointment_email_template_id')->references('id')->on('notification_templates')->onDelete('cascade');
            $table->foreign('results_sms_template_id')->references('id')->on('notification_templates')->onDelete('cascade');
            $table->foreign('results_email_auto_template_id')->references('id')->on('notification_templates')->onDelete('cascade');
            $table->foreign('mammography_sms_template_id')->references('id')->on('notification_templates')->onDelete('cascade');
            $table->foreign('mammography_email_template_id')->references('id')->on('notification_templates')->onDelete('cascade');
            $table->foreign('results_email_template_id')->references('id')->on('notification_templates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configurations', function(Blueprint $table){

            $table->dropForeign('configurations_appointment_sms_template_id_foreign');
            $table->dropForeign('configurations_appointment_email_template_id_foreign');
            $table->dropForeign('configurations_results_sms_template_id_foreign');
            $table->dropForeign('configurations_results_email_auto_template_id_foreign');
            $table->dropForeign('configurations_mammography_sms_template_id_foreign');
            $table->dropForeign('configurations_mammography_email_template_id_foreign');

        });
    }
}