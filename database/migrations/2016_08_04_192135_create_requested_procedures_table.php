<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestedProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requested_procedures', function (Blueprint $table) {

            $table->increments('id');
            $table->boolean('active');
            $table->integer('service_request_id')->unsigned()->index();
            $table->integer('procedure_id')->unsigned()->index();
            $table->string('study_instance_id', 45)->nullable()->unique();
            $table->integer('requested_procedure_status_id')->unsigned()->index();
            $table->boolean('print_label');
            $table->string('study_number', 45);
            $table->integer('equipment_id')->nullable()->unsigned()->index();
            
            $table->integer('technician_user_id')->unsigned()->index();
            $table->string('technician_user_name', 45);
            
            $table->integer('radiologist_user_id')->unsigned()->index();
            $table->string('radiologist_user_name', 45);
            
            $table->integer('transcriptor_user_id')->unsigned()->index();
            $table->string('transcriptor_user_name', 45);
            
            $table->integer('approve_user_id')->unsigned()->index();
            $table->string('approve_user_name', 45);
            
            $table->integer('reject_user_id')->unsigned()->index();
            $table->string('reject_user_name', 45);
            $table->text('reject_reason');
            
            $table->integer('number_of_plates')->unsigned();
            $table->integer('plates_size_id')->unsigned()->index();
            $table->string('nurse', 45);
            $table->boolean('deliver_results');
            $table->boolean('force_images');
            $table->text('comments');
            
            $table->integer('suspension_user_id')->unsigned()->index();
            $table->string('suspension_user_name', 45);
            $table->integer('suspension_reason_id')->nullable()->unsigned()->index();
            
            $table->boolean('urgent');
            $table->boolean('blocked_status');
            $table->integer('blocking_user_id')->unsigned()->index();
            $table->string('blocking_user_name', 45);
            $table->dateTime('technician_end_date');
            $table->dateTime('dictation_date');
            $table->dateTime('transcription_date');
            $table->dateTime('approval_date');
            $table->dateTime('culmination_date');
            $table->dateTime('reject_date');
            $table->dateTime('suspension_date');
            $table->integer('bi_rad_id')->unsigned()->nullable()->index();
            $table->boolean('bi_rad_notified')->default(false)->comment = "Indicates that a reminder notification was sent to redo the test";
            $table->text('text');
            $table->integer('biopsy_result')->unsigned()->index();
            $table->boolean('teaching_file');
            $table->text('teaching_file_text');
            $table->integer('category_id')->unsigned()->index();
            $table->integer('sub_category_id')->unsigned()->index();
            $table->string('dictation_file', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('requested_procedures');
        Schema::enableForeignKeyConstraints();
    }
}
