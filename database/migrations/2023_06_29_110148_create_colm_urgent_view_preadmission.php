<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmUrgentViewPreadmission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->dropView();
        $this->createView();
    }

    public function createView()
    {
        DB::statement('
        
            CREATE VIEW preadmissionIndexView AS
            SELECT 
                `apimeditron`.`patient_types`.`icon` AS `patientTypeIcon`,
                `apimeditron`.`patient_types`.`description` AS `patientType`,
                `apimeditron`.`patient_types`.`administrative_ID` AS `patientTypeAID`,
                `apimeditron`.`requested_procedures`.`id` AS `orderID`,
                `apimeditron`.`requested_procedures`.`service_request_id` AS `serviceRequestID`,
                CONCAT(`apimeditron`.`service_requests`.`patient_first_name`,\' \',`apimeditron`.`service_requests`.`patient_last_name`) AS `patientName`,
                `apimeditron`.`service_requests`.`patient_identification_id` AS `patientIdentificationID`,
                `apimeditron`.`procedures`.`id` AS `procedureID`,
                `apimeditron`.`procedures`.`administrative_ID` AS `procedureAID`,
                `apimeditron`.`procedures`.`description` AS `procedureDescription`,
                `apimeditron`.`service_requests`.`patient_id` AS `patientID`,
                `apimeditron`.`requested_procedure_statuses`.`description` AS `orderStatus`,
                `apimeditron`.`requested_procedure_statuses`.`id` AS `orderStatusID`,
                `apimeditron`.`referrings`.`administrative_ID` AS `referringID`,
                CONCAT(`apimeditron`.`referrings`.`first_name`,\' \',`apimeditron`.`referrings`.`last_name`) AS `referringName`,
                `apimeditron`.`requested_procedures`.`urgent` AS `urgent`
            FROM
                (((((`apimeditron`.`requested_procedures`
                JOIN `apimeditron`.`service_requests` ON ((`apimeditron`.`requested_procedures`.`service_request_id` = `apimeditron`.`service_requests`.`id`)))
                JOIN `apimeditron`.`procedures` ON ((`apimeditron`.`requested_procedures`.`procedure_id` = `apimeditron`.`procedures`.`id`)))
                JOIN `apimeditron`.`requested_procedure_statuses` ON ((`apimeditron`.`requested_procedure_statuses`.`id` = `apimeditron`.`requested_procedures`.`requested_procedure_status_id`)))
                JOIN `apimeditron`.`patient_types` ON ((`apimeditron`.`service_requests`.`patient_type_id` = `apimeditron`.`patient_types`.`id`)))
                LEFT JOIN `apimeditron`.`referrings` ON ((`apimeditron`.`service_requests`.`referring_id` = `apimeditron`.`referrings`.`id`)))
            WHERE
                ((`apimeditron`.`requested_procedure_statuses`.`id` = 1)
                    AND (`apimeditron`.`patient_types`.`admin_aprob` = 1))
            ORDER BY `apimeditron`.`requested_procedures`.`service_request_id`
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    
    public function dropView()
    {
        DB::statement('DROP VIEW IF EXISTS preadmissionIndexView');
    }
     public function down()
    {
        $this->dropView();
    }
}
