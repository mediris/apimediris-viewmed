<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmInstitutionIdTecnicoIndex extends Migration{

    public function up(){

        $this->dropView();
        $this->createView();
    }
    
  public function createView(){
        DB::statement('
        
            CREATE VIEW technicianIndexView AS
              SELECT 
                   `requested_procedures`.`id` AS `orderID`,
                   `requested_procedures`.`service_request_id` AS `serviceRequestID`,
                   `requested_procedures`.`blocked_status` AS `blockedStatus`,
                   `requested_procedures`.`blocking_user_id` AS `blockingUserID`,
                   `requested_procedures`.`blocking_user_name` AS `blockingUserName`,
                   `requested_procedures`.`technician_user_name` AS `technicianUserName`,
                   `requested_procedures`.`radiologist_user_name` AS `radiologistUserName`,
                   `requested_procedures`.`transcriptor_user_name` AS `transcriptorUserName`,
                   `requested_procedures`.`approve_user_name` AS `approveUserName`,
                   `requested_procedure_statuses`.`description` AS `orderStatus`,
                   `requested_procedure_statuses`.`id` AS `orderStatusID`,
                   `requested_procedures`.`technician_user_id` AS `technicianUserID`,
                   `service_requests`.`issue_date` AS `serviceIssueDate`,
                   `procedures`.`description` AS `procedureDescription`,
                   `patient_types`.`description` AS `patientType`,
                   `patient_types`.`icon` AS `patientTypeIcon`,
                   `service_requests`.`patient_first_name` AS `patientFirstName`,
                   `service_requests`.`patient_last_name` AS `patientLastName`,
                   `service_requests`.`patient_identification_id` AS `patientIdentificationID`,
                   `service_requests`.`patient_id` AS `patientID`,
                   `service_requests`.`institution_id` AS `institutionId`,
                   `modalities`.`name` AS `modality`,
                   `requested_procedures`.`urgent` AS `urgent`
              FROM
                    `requested_procedures`
                    JOIN `service_requests` ON (`requested_procedures`.`service_request_id` = `service_requests`.`id`)
                    JOIN `procedures` ON (`requested_procedures`.`procedure_id` = `procedures`.`id`)
                    JOIN `requested_procedure_statuses` ON (`requested_procedure_statuses`.`id` = `requested_procedures`.`requested_procedure_status_id`)
                    JOIN `patient_types` ON (`service_requests`.`patient_type_id` = `patient_types`.`id`)
                    JOIN `modalities` ON (`procedures`.`modality_id` = `modalities`.`id`)
              WHERE
                  (`requested_procedure_statuses`.`id` = 2)
                        
              ORDER BY 
                  serviceRequestID ASC    
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function dropView(){

        DB::statement('DROP VIEW IF EXISTS technicianIndexView');
    }
     public function down(){
        
        $this->dropView();
    }
}
