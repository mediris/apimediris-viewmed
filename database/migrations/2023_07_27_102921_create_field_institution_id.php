<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldInstitutionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('service_requests', function (Blueprint $table) {

            $table->integer('institution_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_requests', function (Blueprint $table) {

            if (Schema::hasColumn('service_requests', 'institution_id')) {
                $table->dropColumn('institution_id');
            }

        });
    }
}
