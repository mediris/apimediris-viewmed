<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmsDataPreadmissionRequestedProcedureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requested_procedures', function (Blueprint $table) {

            $table->integer('preadmission_user_id')->unsigned()->index();
            $table->string('preadmission_user_name', 45);
            $table->dateTime('preadmission_end_date');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requested_procedures', function (Blueprint $table) {

            $table->dropColumn('preadmission_user_id');
            $table->dropColumn('preadmission_user_name');
            $table->dropColumn('preadmission_end_date');

        });
    }
}
