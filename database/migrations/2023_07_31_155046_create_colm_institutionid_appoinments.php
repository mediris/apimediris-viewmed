<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmInstitutionidAppoinments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('appointments', function (Blueprint $table) {

            $table->integer('institution_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('appointments', function (Blueprint $table) {

        if (Schema::hasColumn('appointments', 'institution_id')) {
                $table->dropColumn('institution_id');
            }

        });
    }
}
