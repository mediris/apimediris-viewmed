<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmDocumentTypeViewTranscribeIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        $this->createView();
    }

    public function createView(){

        DB::statement('
        
            CREATE VIEW transcribeIndexView AS
               SELECT
                   `requested_procedures`.`urgent` AS `isUrgent`,
                   `requested_procedures`.`reject_user_id` AS `rejectUserID`,
                   `requested_procedures`.`reject_user_name` AS `rejectUserName`,
                   `patient_types`.`icon` AS `patientTypeIcon`,
                   `patient_types`.`description` AS `patientType`,
                   `requested_procedures`.`dictation_date` AS `dictationDate`,
                   `requested_procedures`.`service_request_id` AS `serviceRequestID`,
                   `requested_procedures`.`id` AS `orderID`,
                   `procedures`.`description` AS `procedureDescription`,
                   concat(`service_requests`.`patient_first_name`,\' \',`service_requests`.`patient_last_name`)  AS `patientName`,
                    (CASE
                        WHEN `patients`.`document_type` IS NULL OR `patients`.`document_type` = \' \' THEN `service_requests`.`patient_identification_id`
                        ELSE CONCAT(`patients`.`document_type`, \' - \', `service_requests`.`patient_identification_id`)
                    END) AS `patientID`,
                   `requested_procedures`.`radiologist_user_id` AS `radiologistUserID`,
                   `requested_procedures`.`radiologist_user_name` AS `radiologistUserName`,
                   `service_requests`.`institution_id` AS `institutionId`
                   
               FROM
                   (((((`requested_procedures`
                   JOIN `service_requests` ON ((`requested_procedures`.`service_request_id` = `service_requests`.`id`)))
                   JOIN `procedures` ON ((`requested_procedures`.`procedure_id` = `procedures`.`id`)))
                   JOIN `mediris`.`patients` ON ((`service_requests`.`patient_identification_id` = `patients`.`patient_ID`)))
                   JOIN `requested_procedure_statuses` ON ((`requested_procedure_statuses`.`id` = `requested_procedures`.`requested_procedure_status_id`)))
                   JOIN `patient_types` ON ((`service_requests`.`patient_type_id` = `patient_types`.`id`)))
               WHERE
                   (`requested_procedure_statuses`.`id` = 4)
            
        ');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS transcribeIndexView');
    }
}
