<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysRequestDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_documents', function (Blueprint $table) {

            $table->foreign('service_request_id')->references('id')->on('service_requests')->onDelete('cascade'); //delete files associates to a service_request

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_documents', function (Blueprint $table) {

            $table->dropForeign('request_documents_service_request_id_foreign');

        });
    }
}
