<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysProcedureRoom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('procedure_room', function (Blueprint $table) {

            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->foreign('procedure_id')->references('id')->on('procedures')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('procedure_room', function (Blueprint $table) {

            $table->dropForeign('procedure_room_room_id_foreign');
            $table->dropForeign('procedure_room_procedure_id_foreign');

        });
    }
}
