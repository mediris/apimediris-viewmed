<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmReferenceNumberRequestedProcedures extends Migration{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('requested_procedures', function (Blueprint $table) {
            $table->string('reference_number', 100);
        });
    }

    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        if (Schema::hasColumn('requested_procedures', 'reference_number')) {
            $table->dropColumn('reference_number');
        }
    }
}
