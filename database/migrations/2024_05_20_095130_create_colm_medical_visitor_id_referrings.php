<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmMedicalVisitorIdReferrings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('referrings', function (Blueprint $table) {
            $table->integer('medical_visitor_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('referrings', function (Blueprint $table) {

        if (Schema::hasColumn('referrings', 'medical_visitor_id')) {
                
            $table->dropColumn('medical_visitor_id');

            }
        });
    }
}
