<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeysRadiologistBlockingAll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requested_procedures', function (Blueprint $table) {

            $table->integer('blocking_user_id_radiologist')->nullable();
            $table->string('blocking_user_name_radiologist')->nullable();
            $table->integer('block_radiologist_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    
        Schema::table('requested_procedures', function (Blueprint $table) {

            $table->dropColumn('blocking_user_id_radiologist');
            $table->dropColumn('blocking_user_name_radiologist');
            $table->dropColumn('block_radiologist_status');

        });  
    }
}
