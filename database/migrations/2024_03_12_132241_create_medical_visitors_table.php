<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::create('medical_visitors', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active');
            $table->string('last_name', 50)->index();
            $table->string('first_name', 50)->index();
            $table->string('address', 250);
            $table->string('telephone_number', 25)->index();
            $table->string('telephone_number_2', 25);
            $table->string('cellphone_number', 25)->index();
            $table->string('cellphone_number_2', 25);
            $table->string('email', 50)->unique()->index();
            $table->string('administrative_ID', 10)->unique()->index();
            $table->string('medical_visitor_institution', 250);
            $table->string('speciality', 250);
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->index(['first_name', 'last_name']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('medical_visitors');
    }
}
