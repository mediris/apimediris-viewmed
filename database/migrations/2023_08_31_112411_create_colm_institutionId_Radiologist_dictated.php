<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmInstitutionIdRadiologistDictated extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->dropView();
        $this->createView();
    }

    public function createView()
    {
        DB::statement('
        
            CREATE VIEW `apimeditron`.`radiologist_to_transcribe` AS
        SELECT 
            `apimeditron`.`requested_procedures`.`id` AS `orderID`,
            `apimeditron`.`requested_procedures`.`service_request_id` AS `serviceRequestID`,
            `apimeditron`.`requested_procedures`.`blocked_status` AS `blockedStatus`,
            `apimeditron`.`requested_procedures`.`blocking_user_id` AS `blockingUserID`,
            `apimeditron`.`requested_procedures`.`radiologist_user_id` AS `radiologistUserID`,
            `apimeditron`.`requested_procedures`.`blocking_user_name` AS `blockingUserName`,
            `apimeditron`.`requested_procedures`.`technician_user_name` AS `technicianUserName`,
            `apimeditron`.`requested_procedures`.`radiologist_user_name` AS `radiologistUserName`,
            `apimeditron`.`requested_procedures`.`transcriptor_user_name` AS `transcriptorUserName`,
            `apimeditron`.`requested_procedures`.`approve_user_name` AS `approveUserName`,
            `apimeditron`.`requested_procedure_statuses`.`description` AS `orderStatus`,
            `apimeditron`.`requested_procedure_statuses`.`id` AS `orderStatusID`,
            `apimeditron`.`service_requests`.`issue_date` AS `serviceIssueDate`,
            `apimeditron`.`requested_procedures`.`dictation_date` AS `serviceDictationDate`,
            `apimeditron`.`procedures`.`description` AS `procedureDescription`,
            `apimeditron`.`patient_types`.`description` AS `patientType`,
            `apimeditron`.`patient_types`.`icon` AS `patientTypeIcon`,
            `apimeditron`.`service_requests`.`patient_first_name` AS `patientFirstName`,
            `apimeditron`.`service_requests`.`patient_last_name` AS `patientLastName`,
            `apimeditron`.`service_requests`.`patient_identification_id` AS `patientID`,
            `apimeditron`.`requested_procedures`.`approve_user_id` AS `approveID`,
            `apimeditron`.`patient_types`.`color` AS `patientTypeColor`,
            `apimeditron`.`service_requests`.`institution_id` AS `institutionId`,
            `apimeditron`.`requested_procedures`.`urgent` AS `urgent`
        FROM
            ((((`apimeditron`.`requested_procedures`
            JOIN `apimeditron`.`service_requests` ON ((`apimeditron`.`requested_procedures`.`service_request_id` = `apimeditron`.`service_requests`.`id`)))
            JOIN `apimeditron`.`procedures` ON ((`apimeditron`.`requested_procedures`.`procedure_id` = `apimeditron`.`procedures`.`id`)))
            JOIN `apimeditron`.`requested_procedure_statuses` ON ((`apimeditron`.`requested_procedure_statuses`.`id` = `apimeditron`.`requested_procedures`.`requested_procedure_status_id`)))
            JOIN `apimeditron`.`patient_types` ON ((`apimeditron`.`service_requests`.`patient_type_id` = `apimeditron`.`patient_types`.`id`)))
        WHERE
            (`apimeditron`.`requested_procedure_statuses`.`id` = 6)
       
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function dropView()
    {
        DB::statement('DROP VIEW IF EXISTS radiologist_to_transcribe');
    }
     public function down(){
        
        $this->dropView();
    }
}
