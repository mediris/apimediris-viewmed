<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewKeysTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        $this->down();

        Schema::table('procedure_template', function (Blueprint $table) {
            $table->integer('user_id')->nullable()->unsigned()->index();
            $table->integer('modality_id')->nullable()->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){

        Schema::table('procedure_template', function (Blueprint $table) {
        if (Schema::hasColumn('procedure_template', 'user_id', 'modality_id')) {
                $table->dropColumn('user_id');
                $table->dropColumn('modality_id');
            }
        });
    }
}
