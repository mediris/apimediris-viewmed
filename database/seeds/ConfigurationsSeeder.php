<?php

use Illuminate\Database\Seeder;
use App\Configuration;

class ConfigurationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Configuration::create([
            'appointment_sms_notification' => 1,
            'birthday_expression' => '/**/',
            'mammography_expression' => '/**/',
            'mammography_reminder' => 1,
            'appointment_sms_expression' => '/**/',

            'appointment_sms'   => true,
            'appointment_sms_template_id' => 2,
            
            'appointment_email' => true,
            'appointment_email_template_id' => 1,

            'results_sms'     => true,
            'results_sms_template_id' => 4,

            'results_email_auto'=> true,
            'results_email_auto_template_id' => 3,
            'results_email_referer_template_id' => 7,

            'mammography_sms'   => true,
            'mammography_sms_template_id' => 6,

            'mammography_email' => true,
            'mammography_email_template_id' => 5,

            'results_email' => true,
            'results_email_template_id' => 8,

            'active_poll' => false,
            'url_poll'=>' ',
            'poll_email_template_id' => 8
        ]);
    }
}
