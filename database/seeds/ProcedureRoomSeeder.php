<?php

use Illuminate\Database\Seeder;
use App\Room;
use App\Procedure;

use Maatwebsite\Excel\Facades\Excel;

class ProcedureRoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // se carga el seed a partir del archivo procedimiento_posible.csv
        Excel::load('database/exportFile/procedimiento_posible.csv', function ( $reader ){
            foreach( $reader->get() as $seed ){
                // se obtiene el procedure especifico a traves del campo oldId
                $procedure = Procedure::where('oldId', $seed->id_procedimiento)->get();
                // se obtiene el room especifico a traves del campo oldId
                $room = Room::where('oldId', $seed->id_sala)->get();
                if( ( isset( $procedure[0] ) ) && ( isset( $room[0] ) ) ){
                    /* se incluye en la tabla procedure_room el id del room y el id del procedure, con el fin
                    de relacionar las tablas Procedures y Rooms a traves de la tabla procedure_room  */
                    //Room::find($room[0]->id)->procedures()->attach($procedure[0]->id, ['duration' => $seed->cuposnecesarios]);
                    Room::find($room[0]->id)->procedures()->attach($procedure[0]->id, ['duration' => 1]);
                }
            }
        });

    }
}
