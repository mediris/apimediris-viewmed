<?php

use Illuminate\Database\Seeder;
use App\PatientState;

class PatientStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['Estable', 'En coma'];


        PatientState::create([
            'id' => 1,
            'active' => 1,
            'description' => 'Estable',
            'language' => 'es'
        ]);

            PatientState::create([
            'id' => 2,
            'active' => 1,
            'description' => 'En coma',
            'language' => 'es'
        ]);
    }
}