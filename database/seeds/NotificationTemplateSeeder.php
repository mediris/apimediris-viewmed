<?php

use Illuminate\Database\Seeder;
use App\NotificationTemplate;

class NotificationTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $values = [
        	[
        		"Nueva Cita Pautada",
        		"Recuerde su cita el {{date}} en IDACA-C.C. Oasis. Para más información o anular la cita, llame al 0501-IDACAVE (0501-4322283).Favor traer orden médica"
        	],
        	[
        		"Nueva Cita Pautada SMS",
        		"Recuerde su cita el {{date}} en IDACA-C.C. Oasis. Para más información o anular la cita, llame al 0501-IDACAVE (0501-4322283).Favor traer orden médica"
        	],
        	[
        		"Resultados de estudio",
        		"Estimado paciente, su estudio de {{procedure}} en IDACA-C.C. Oasis ya está informado. Puede retirarlo de lun a vie de 7am a 7pm, horario corrido."
        	],
        	[
        		"Resultados de estudio SMS",
        		"Estimado paciente, su estudio de {{procedure}} en IDACA-C.C. Oasis ya está informado. Puede retirarlo de lun a vie de 7am a 7pm, horario corrido."
        	],
        	[
        		"Examen de control",
        		"Estimado paciente, IDACA-Centro Medico Docente La Trinidad le recuerda que se acerca la fecha de su control de mamografía. Para citas llame al 0501- IDACAVE (0501-4322283)"
        	],
        	[
        		"Examen de control SMS",
        		"Estimado paciente, IDACA-Centro Medico Docente La Trinidad le recuerda que se acerca la fecha de su control de mamografía. Para citas llame al 0501- IDACAVE (0501-4322283)"
        	],
            [
                "Resultado de estudio (referido)",
                "Estimado Doctor, el estudio {{procedure}} del paciente {{patient_name}} en IDACA-C.C. Oasis ya está informado."
            ],
            [
                "Resultados por correo",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            ],
            [
                "Envio de Encuesta",
                "Estimado Paciente, Con el fin de siempre mejorar nuestra atención, queremos conocer su opinión sobre el servicio que ofrecimos en su última visita a IDACA. Por favor, permítanos unos minutos de su tiempo para completar la siguiente encuesta {{link_poll}}"
            ]
        ];

        foreach($values as $key => $value) {
            NotificationTemplate::create([
                'description' 	=> $value[0],
                'template'		=> $value[1],
                'active'   		=> true
            ]);
        }
    }
}
