<?php

use Illuminate\Database\Seeder;
use App\Step;
use App\Equipment;

class EquipmentStepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // se carga el seed a partir del archivo equipo_procedimiento.csv
        Excel::load('database/exportFile/equipo_procedimiento.csv', function ( $reader ){
            foreach( $reader->get() as $seed ){

                // Se obtiene el step_id de la tabla procedure_step
                $step = DB::table('procedures')
                    ->select(DB::raw("procedure_step.step_id"))
                    ->join('procedure_step','procedures.id','=','procedure_step.procedure_id')
                    ->where([
                        ['procedures.oldId','=',$seed->id_procedimiento]
                    ])
                    ->get();

                // se verifica que exista el $step
                if ( count($step) != 0 ) {
                    $equipment = Equipment::where('oldId', $seed->id_equipo)->get();

                    $veri = DB::table('equipment_step')
                        ->select(DB::raw("equipment_step.step_id"))
                        ->where([
                            ['equipment_step.step_id','=',$step[0]->step_id],
                            ['equipment_step.equipment_id','=',$equipment[0]->id]
                        ])
                        ->get();
                        // si el $veri no obtiene ningun resultado
                        if (count($veri) == 0)
                            Step::find($step[0]->step_id)->equipment()->attach([$equipment[0]->id]);
                }
            }
        });
    }
}
