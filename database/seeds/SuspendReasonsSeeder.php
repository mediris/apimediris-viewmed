<?php

use Illuminate\Database\Seeder;
use App\SuspendReason;

class SuspendReasonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $suspendreasons = ['Cita mal otorgada'
        //         ,'Equipo dañado'
        //         ,'Es claustrofobico'
        //         ,'Es alérgico'
        //         ,'Estudio mal cargado por la administración (recepción)'
        //         ,'Falla eléctrica, de sistema o del equipo'
        //         ,'Falta de Insumo'
        //         ,'Incluido en otro estudio'
        //         ,'Medico Radiólogo suspende el caso porque amerita otro estudio'
        //         ,'Médico Tratante omite el estudio'
        //         ,'Médico tratante solicito cambio del estudio'
        //         ,'No cumple con la preparación del estudio'
        //         ,'No había médico'
        //         ,'No había técnico'
        //         ,'No resistió el estudio'
        //         ,'Paciente cargado doble'
        //         ,'Paciente con material metálico dentro del cuerpo'
        //         ,'Paciente dado de alta'
        //         ,'Paciente falleció'
        //         ,'Paciente no cumplía con la papelería del convenio'
        //         ,'Paciente no quiso esperar vendrá otro día'
        //         ,'Paciente no tenía el pago del estudio'
        //         ,'Paciente se descompensa en el momento del estudio'
        //         ,'Presenta reacción alérgica  al contraste en el momento del estudio'
        //     ];

        $suspendReasonsAdmin = [
            'Cita mal otorgada',
            'Estudio mal cargado por la administración (recepción)',
            'Paciente cargado doble',
            'Paciente no tenía el pago del estudio',
            'Paciente no cumplía con la papelería del convenio',
            'Paciente no quiso esperar vendrá otro día',
            'No cumple con la preparación del estudio',
            'No había médico',
            'No había técnico',
            'Incluido en otro estudio',
            'Datos errados del paciente',
            'Datos errados del estudio (s)',
            'Paciente no presenta los originales del depósito (S) o de la retención',
        ];

        $suspendReasonTechnical = [
            'Falta de Insumo',
            'Es alérgico',
            'Es  claustrofobico',
            'No resistió el estudio',
            'Presenta reacción alérgica  al contraste en el momento del estudio',
            'Paciente se descompensa en el momento del estudio',
            'Paciente falleció',
            'Paciente dado de alta',
            'Médico Tratante omite el estudio',
            'Médico tratante solicito cambio del estudio',
            'Equipo dañado',
            'Medico Radiólogo suspende el caso porque amerita otro estudio',
            'Paciente con material metálico dentro del cuerpo',
            'Falla eléctrica, de sistema  o del equipo',
            'Paciente supera el peso límite del equipo',
            'Estudio mal realizado por el técnico del equipo',
            'Suspendido por falla en el Aire Acondicionado en la sala del equipo'
        ];


        foreach($suspendReasonsAdmin as $suspendreason)
        {
            SuspendReason::create([
                'description' => $suspendreason,
                'active' => 1,
                'admin_reason' => true
            ]);
        }

        foreach($suspendReasonTechnical as $suspendreason)
        {
            SuspendReason::create([
                'description' => $suspendreason,
                'active' => 1,
                'admin_reason' => false
            ]);
        }

    }
}
