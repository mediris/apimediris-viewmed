<?php

use Illuminate\Database\Seeder;
use App\Referring;

class ReferringsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Referring::create([
            'active' => 1,
            'first_name' => 'No',
            'last_name' => 'Especificado',
            'address' => 'none',
            'telephone_number' => '963576985',
            'cellphone_number' => '963576985',
            'email' => 'noespecificado@noespecificado.com',
            'administrative_ID' => 'REFERRING1',
        ]);
    }
}
