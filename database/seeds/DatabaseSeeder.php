<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(InitialSeeder::class );
        $this->call(NotificationTemplateSeeder::class);
        $this->call(ConfigurationsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(ReferringsSeeder::class);
        $this->call(DivisionsSeeder::class);
        $this->call(ModalitiesSeeder::class);
        $this->call(RoomsSeeder::class);
        $this->call(EquipmentSeeder::class);
        $this->call(TemplatesSeeder::class);
        $this->call(StepsSeeder::class);
        $this->call(ProceduresSeeder::class);
        $this->call(PregnancyStatusSeeder::class);
        $this->call(SmokingStatusSeeder::class);
        $this->call(RequestStatusSeeder::class);
        $this->call(AnswersSeeder::class);
        $this->call(PatientTypesSeeder::class);
        $this->call(SourcesSeeder::class);
        $this->call(UnitsSeeder::class);
        $this->call(AppointmentStatusesSeeder::class);
        $this->call(PatientStateSeeder::class);
        $this->call(RequestedProcedureStatusesSeeder::class);
        $this->call(ProcedureRoomSeeder::class);
        $this->call(PlatesSizesSeeder::class);
        $this->call(BiRadsSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(SubCategoriesSeeder::class);
        $this->call(SuspendReasonsSeeder::class);
        $this->call(ProcedureTemplateSeeder::class);
        $this->call(ProcedureStepSeeder::class);
        $this->call(EquipmentStepSeeder::class);
        $this->call(FinalSeeder::class);
    }
}
