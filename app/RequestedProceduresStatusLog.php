<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestedProceduresStatusLog extends Model
{   
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'requested_procedures_status_log';
    /**
     * @fecha: 11-10-2017
     * @programador: Ricardo Martos
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'requested_procedure_id',
        'old_status',
        'new_status',
        'date'
    ];

}
