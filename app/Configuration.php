<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que no pueden ser llenados a través de eloquent (los que no salgan aquí sí podrán ser llenados).
     */
    protected $guarded = ['id', 'api_token', 'user_id'];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para llevar todos los valores booleanos a 0.
     */
    public function setZeros()
    {
        $this->appointment_sms = 0;
        $this->results_sms = 0;
        $this->results_email_auto = 0;
        $this->appointment_email =0;
        $this->birthday_email =0;
        $this->mammography_email = 0;
        $this->mammography_sms = 0;
        $this->results_email = 0;
        $this->save();
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función utilizada en los inputs para determinar si está checked o no.
     */
    public function isChecked($value)
    {
        if($value)
        {
            return "checked";
        }
    }

    public function appointmentEmailTemplate() {
        return $this->belongsTo(NotificationTemplate::class, 'appointment_email_template_id');
    }

    public function appointmentSmsTemplate() {
        return $this->belongsTo(NotificationTemplate::class, 'appointment_sms_template_id');   
    }

    public function resultsEmailAutoTemplate() {
        return $this->belongsTo(NotificationTemplate::class, 'results_email_auto_template_id');
    }

    public function resultsEmailRefererTemplate() {
        return $this->belongsTo(NotificationTemplate::class, 'results_email_referer_template_id');
    }

    public function resultsSmsTemplate() {
        return $this->belongsTo(NotificationTemplate::class, 'results_sms_template_id');   
    }

    public function mammographyEmailTemplate() {
        return $this->belongsTo(NotificationTemplate::class, 'mammography_email_template_id');
    }

    public function mammographySmsTemplate() {
        return $this->belongsTo(NotificationTemplate::class, 'mammography_sms_template_id');
    }     

    public function resultsEmailTemplate() {
        return $this->belongsTo(NotificationTemplate::class, 'results_email_template_id');
    }

    public function pollsEmailTemplate() {
        return $this->belongsTo(NotificationTemplate::class, 'poll_email_template_id');
    }
}
