<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientType extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'description', 'administrative_ID', 'priority', 'active', 'icon','color', 'admin_aprob', 'sms_send', 'email_patient', 'email_refeer', 'parent_id', 'level'
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un PatientType tiene muchos ServiceRequests.
     */
    public function serviceRequests()
    {
        return $this->hasMany(ServiceRequest::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un PatientType tiene un PatientType padre.
     */
    public function parentPatientType()
    {
        return $this->hasOne(PatientType::class, 'id', 'parent_id');
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un PatientType tiene muchos PatientTypes hijos.
     */
    public function childPatientTypes()
    {
        return $this->hasMany(PatientType::class, 'parent_id', 'id');
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para llevar todos los valores booleanos a 0.
     */
    public function setZeros()
    {
        $this->active = 0;
        $this->admin_aprob = 0;
        $this->sms_send = 0;
        $this->email_patient = 0;
        $this->email_refeer = 0;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active()
    {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
    
}
