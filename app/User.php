<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * @IMPORTANTE: Este modelo alberga sólo el api_token de los usuarios que tienen acceso a esta api. 
     */
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =[
        'administrative_ID', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un User tiene un Referring.
     */
    public function referring()
    {
        return $this->hasOne(Referring::class);
    }
}
