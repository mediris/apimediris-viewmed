<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalVisitor extends Model{
    
    /**
     * @fecha: 27-02-2024
     * @programador: Billy Gonzalez
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'last_name', 'first_name', 'address', 'telephone_number', 'telephone_number_2', 'cellphone_number', 'cellphone_number_2', 'email', 'administrative_ID', 'user_id', 'active', 'medical_visitor_institution', 'speciality',
    ];

    /**
     * @fecha: 27-02-2024
     * @programador: Billy Gonzalez
     * @objetivo: Relación: Un Referring tiene muchos ServiceRequests.
     */
    public function serviceRequests(){
        return $this->hasMany(ServiceRequest::class);
    }

    /**
     * @fecha: 27-02-2024
     * @programador: Billy Gonzalez
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active(){
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}
