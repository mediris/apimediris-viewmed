<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestedProcedure;
use App\Http\Requests;
use App\Worklist;
use Activity;
use Log;
use DB;
use DataSourceResult;

class TechnicianController extends Controller
{

    public function __construct()
    {
        $host = config('database.connections.mysql.host');
        $dbname = config('database.connections.mysql.database');
        $conn = config('database.default');
        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');

        $this->DbHandler = new DataSourceResult("$conn:host=$host;dbname=$dbname", $username, $password);
    }

    public function index(Request $request)
    {
        try
        {
            
            $fields = [
                'patientTypeIcon',
                'patientType',
                'orderID',
                'serviceRequestID',
                'serviceIssueDate',
                'procedureDescription',
                'patientFirstName',
                'patientLastName',
                'patientIdentificationID',
                'patientID',
                'blockedStatus',
                'blockingUserID',
                'blockingUserName',
                'TechnicianUserID',
                'orderStatus',
                'orderStatusID',
                'institutionId',
                'modality',
                'urgent',
            ];


            $req = $request->all();

            $data = (object) $req['data'];

            if(isset($data->filter))
            {
                $data->filter = (object) $data->filter;
                foreach ($data->filter->filters as $key => $filter)
                {
                    $data->filter->filters[$key] = (object) $filter;
                }
            }

            if(isset($data->sort)) {
                foreach ($data->sort as $key => $sortArray) {
                    $data->sort[$key] = (object) $sortArray;
                }
            }

            $result = $this->DbHandler->read('technicianIndexView', $fields, $data);
            
            return response()->json(['code' => '200', 'data' => $result]);      

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/technician/technician.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Technician. Action: index');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    public function show(Request $request, RequestedProcedure $requestedProcedure)
    {
        try
        {

            $requestedProcedure->load(['procedure', 'serviceRequest', 'requestedProcedureStatus', 'platesSize']);
            $requestedProcedure->serviceRequest->load(['patientType']);

            /**
             * Log activity
             */
            Activity::log(trans('tracking.show', ['section' => 'technician', 'id' => $requestedProcedure->id]), $request->all()['user_id']);

            return $requestedProcedure;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/technician/technician.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: technician. Action: show');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    public function edit(Request $request, RequestedProcedure $requestedProcedure)
    {

        if($request->isMethod('POST')) {

            try
            {
            
                $requestedProcedure->load(['procedure', 'serviceRequest', 'requestedProcedureStatus', 'platesSize']);

                /**
                 * Log activity
                 */
                Activity::log(trans('tracking.show', ['section' => 'technician', 'id' => $requestedProcedure->id]), $request->all()['user_id']);
                
                return response()->json(['code' => 200, 'requestedProcedure' => $requestedProcedure ]);
            
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/technician/technician.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: technician. Action: show');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }

        }
    }

    public function active(Request $request, RequestedProcedure $requestedProcedure)
    {
        try
        {

            $original = new RequestedProcedure();
            foreach($requestedProcedure->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $requestedProcedure->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'technician', 'id' => $requestedProcedure->id, 'oldValue' => $original, 'newValue' => $requestedProcedure, 'action' => 'active']), $request->all()['user_id']);


        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/technician/technician.log');
            Log::alert('File: ' . $e->getFile() . ' Error Line: ' . $e->getLine() . ' Error code: '.$e->getCode().' Error message: '.$e->getMessage(). ' Section: technician. Action: active');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }

        $requestedProcedure->load(['serviceRequest', 'procedure']);
        $original->load(['serviceRequest', 'procedure']);

        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $requestedProcedure]);

    }
}
