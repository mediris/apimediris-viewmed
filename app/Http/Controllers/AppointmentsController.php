<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Appointment;
use App\AppointmentStatus;
use App\Http\Requests;
use App\Room;
use DB;
use Activity;
use Log;
use DataSourceResult;

class AppointmentsController extends Controller
{
    
    public function __construct()
    {
        $host = config('database.connections.mysql.host');
        $dbname = config('database.connections.mysql.database');
        $conn = config('database.default');
        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');
        
        $this->DbHandler = new DataSourceResult("$conn:host=$host;dbname=$dbname", $username, $password);
    }
    
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Appointments.
     */
    public function index(Request $request)
    {
        try
        {
            $fields = [
               'id',
               'patientID',
               'patientIdentificationID',
               'lastName',
               'firstName',
               'procedureName',
               'room',
               'date',
               'dateStartTime',
               'dateEndTime',
               'statusDescription',
               'statusColor',
               'institutionId'
            ];
            
            $req = $request->all();
    
            $data = (object) $req['data'];
    
            if(isset($data->filter))
            {
                $data->filter = (object) $data->filter;
                foreach ($data->filter->filters as $key => $filter)
                {
                    $data->filter->filters[$key] = (object) $filter;
                }
            }
  
            if(isset($data->sort)) {
                foreach ($data->sort as $key => $sortArray) {
                    $data->sort[$key] = (object) $sortArray;
                }
            }

 
            $result = $this->DbHandler->read('receptionIndexView', $fields, $data);

            return response()->json(['code' => '200', 'data' => $result ]);

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/appointments/appointments.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Appointments. Action: index');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de Appointment.
     */
    public function show(Request $request, Appointment $appointment)
    {
        try
        {
            $appointment->load(['room', 'procedure', 'appointmentStatus']);
            $appointment->procedure->load('modality');
            /**
             * Log activity
             */
            Activity::log(trans('tracking.show', ['section' => 'appointments', 'id' => $appointment->id]), $request->all()['user_id']);
            return $appointment;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/appointments/appointments.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: appointments. Action: show');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    /**
     * @fecha: 25-10-2017
     * @parametros: 
     * @programador: Hendember Heras
     * @objetivo: Devuelve todos los Appointments que esten en status created.
     */    
    public function showCreated(Request $request) {
        $status = AppointmentStatus::where('description', 'created')->first();
        $appointments = Appointment::where('appointment_status_id', $status->id)->get();

        return $appointments;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para agregar un nuevo Appointment.
     */
    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'room_id' => 'required',
                'procedure_id' => 'required',
                'patient_identification_id' => 'required',
                'patient_email' => 'required|email',
                'patient_first_name' => 'required',
                'patient_last_name' => 'required',
                //'patient_telephone_number' => 'required',
                //'patient_cellphone_number' => 'required',
                'appointment_date_start' => 'required',
                'appointment_date_end' => 'required',
                'procedure_duration' => 'required',
                'appointment_status_id' => 'required',
                'string_start_unique' => 'required',
                'string_end_unique' => 'required',
                'equipment_id' => 'required',
                'procedure_contrast_study'=> 'required',
            ]);

            $appointment = new Appointment($request->all());

            try
            {
                $appointment->save();

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.create', ['section' => 'appointments', 'id' => $appointment->id]), $request->all()['user_id']);

                return response()->json(['code' => '201', 'message' => 'Created', 'newValue' => $appointment]);
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/appointments/appointments.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: appointments. Action: add');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para editar un Appointment.
     */
    public function edit(Request $request, Appointment $appointment)
    {
       if($request->isMethod('post'))
       {
           $this->validate($request, [
               'room_id' => 'required',
               'procedure_id' => 'required',
               'patient_identification_id' => 'required',
               'patient_email' => 'required|email',
               'patient_first_name' => 'required',
               'patient_last_name' => 'required',
               //'patient_telephone_number' => 'required',
               //'patient_cellphone_number' => 'required',
               'appointment_date_start' => 'required',
               'appointment_date_end' => 'required',
               'procedure_duration' => 'required',
               'appointment_status_id' => 'required',
               'string_start_unique' => 'required',
               'string_end_unique' => 'required',
               'equipment_id' => 'required',
               'procedure_contrast_study'=> 'required',
           ]);

           $original = new Appointment();
           foreach($appointment->getOriginal() as $key => $value)
           {
               $original->$key = $value;
           }

           try
           {
               $appointment->update($request->all());
               /**
                * Log activity
                */
               $appointment->load(['room', 'procedure', 'appointmentStatus']);
               $original->load(['room', 'procedure', 'appointmentStatus']);
               Activity::log(trans('tracking.edit', ['section' => 'appointments', 'id' => $appointment->id, 'oldValue' => $original, 'newValue' => $appointment]), $request->all()['user_id']);
           }
           catch(\Exception $e)
           {
               Log::useFiles(storage_path().'/logs/appointments/appointments.log');
               Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: appointments. Action: edit');
               $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
               return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
           }
           return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $appointment]);
       }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el estado de un appointment.
     */
    public function active(Request $request, Appointment $appointment, $status = null)
    {
        try
        {
            $original = new Appointment();
            foreach($appointment->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $appointment->active($status);

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'appointments', 'id' => $appointment->id, 'oldValue' => $original, 'newValue' => $appointment, 'action' => 'active']), $request->all()['user_id']);


        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/appointments/appointments.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: appointments. Action: active');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }

        $appointment->load(['appointmentStatus', 'room', 'procedure']);
        $original->load(['appointmentStatus', 'room', 'procedure']);

        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $appointment]);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Appointments.
     */
    public function datasource(Request $request)
    {
        $appointments=null;
            $appointments = Appointment::where(
                function($query){
                    $query->where('appointment_status_id',1)
                    ->orWhere('appointment_status_id',2)
                    ->orWhere('appointment_status_id',3);
                })
                ->where('appointment_date_start','>=',Carbon::parse ($request['start_date']  )->startOfDay()->format('Y/m/d H:i:s') )
                ->where('appointment_date_end','<=',Carbon::parse ($request['end_date']  )->endOfDay()->format('Y/m/d H:i:s'))

                //->orwhere('appointment_status_id',1)
                //->orWhere('appointment_status_id', 2)->orWhere('appointment_status_id', 3)
                ->orderBy('appointment_date_start', 'desc')->get();


        $appointments->load(['room', 'procedure', 'appointmentStatus']);
        return $appointments;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Appointments en base a un criterio de búsqueda.
     */
    public function search(Request $request)
    {
        if($request->isMethod('post'))
        {
            try
            {
                $appointments = Appointment::where('appointment_status_id', '!=', 4)
                    ->where(function($q) use($request){
                        $q->where('patient_identification_id', 'like', '%' . $request->input('search_criteria') . '%')
                            ->orwhere('patient_identification_id', 'like', '%' . $request->input('search_criteria') . '%')
                            ->orwhere('patient_first_name', 'like', '%' . $request->input('search_criteria') . '%')
                            ->orwhere('patient_last_name', 'like', '%' . $request->input('search_criteria') . '%')
                            ->orwhere('patient_email', 'like', '%' . $request->input('search_criteria') . '%');
                    })
                    ->orderBy('appointment_date_start', 'desc')->get();

                $appointments->load(['room', 'procedure', 'appointmentStatus']);

                return $appointments;
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/appointments/appointments.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: appointments. Action: search');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }
        }
    }
}
