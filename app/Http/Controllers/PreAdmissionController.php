<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestedProcedure;
use App\ServiceRequest;
use App\Http\Requests;
use Activity;
use Log;
use DB;
use DataSourceResult;

class PreAdmissionController extends Controller
{
    public function __construct()
    {
        $host = config('database.connections.mysql.host');
        $dbname = config('database.connections.mysql.database');
        $conn = config('database.default');
        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');

        $this->DbHandler = new DataSourceResult("$conn:host=$host;dbname=$dbname", $username, $password);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra
     * @objetivo: Retornar una colección en formato Json de RequestedProcedures en base a la vista preadmissionIndexView.
     */
    public function index(Request $request)
    {
        try
        {

            $fields = [
                'patientTypeIcon', 
                'patientType', 
                'orderID', 
                'serviceRequestID', 
                'patientName', 
                'patientIdentificationID',
                'procedureDescription',
                'patientID',
                'orderStatus',
                'orderStatusID',
                'institutionId',
                'procedureID',
                'referringID',
                'referringName',
                'patientTypeAID',
                'procedureAID',
                'urgent',
            ];


            $req = $request->all();

            $data = (object) $req['data'];

            if(isset($data->filter))
            {
                $data->filter = (object) $data->filter;
                foreach ($data->filter->filters as $key => $filter)
                {
                    $data->filter->filters[$key] = (object) $filter;
                }
            }

            if(isset($data->sort)) {
                foreach ($data->sort as $key => $sortArray) {
                    $data->sort[$key] = (object) $sortArray;
                }
            }

            $result = $this->DbHandler->read('preadmissionIndexView', $fields, $data);
            
            return response()->json(['code' => '200', 'data' => $result ]);

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/preadmission/preadmission.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: preadmission. Action: index');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de RequestedProcedure.
     */
    public function show(RequestedProcedure $requestedProcedure, Request $request)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'requestedProcedures', 'id' => $requestedProcedure->id]), $request->all()['user_id']);

        $requestedProcedure->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);

        return $requestedProcedure;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Editar una instancia de RequestedProcedure.
     */
    public function edit(Request $request, RequestedProcedure $requestedProcedure, ServiceRequest $serviceRequest)
    {

        if($request->isMethod('post'))
        {
            // return response()->json(['code' => '200', 'data' => $request->all()['data'], 'rp' => $requestedProcedure]);

            $originalRequestedProcedure = new RequestedProcedure();

            foreach ($requestedProcedure->getOriginal() as $key => $value) {
                
                $originalRequestedProcedure->$key = $value;
            
            }

            $originalServiceRequest = new ServiceRequest();

            foreach ($serviceRequest->getOriginal() as $key => $value) {
                
                $originalServiceRequest->$key = $value;
            
            }

            try
            {

                DB::transaction(function() use($request, $requestedProcedure, $originalRequestedProcedure, $serviceRequest, $originalServiceRequest)
                {

                    $data = $request->all()['data'];

                    $requestedProcedure->update($data['requestedProcedure']);
                    $serviceRequest->update($data['serviceRequest']);


                    $serviceRequest->load('requestedProcedures');
                    $originalServiceRequest->load('requestedProcedures');

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'serviceRequest', 'id' => $serviceRequest->id, 'oldValue' => $originalServiceRequest, 'newValue' => $serviceRequest]), $request->all()['user_id']);
                });
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/requestedProcedures/requestedProcedures.log');
                Log::alert('File: ' . $e->getFile() . ' Error code: ' . $e->getCode() . ' Error Line: ' . $e->getLine() . ' Error message: '. $e->getMessage() . ' Section: requestedProcedures. Action: edit');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $originalServiceRequest, 'newValue' => $serviceRequest]);
        }
    }
}
