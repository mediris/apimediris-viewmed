<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PlatesSize;

use App\Http\Requests;

use Activity;

use Log;

class PlatesSizeController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de PlatesSizes.
     */
    public function index()
    {
        try
        {
            $plates_size = PlatesSize::orderBy('id', 'asc')->get();
            
            return $plates_size;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: referrings. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }
}
