<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Configuration;
use Activity;
use Log;

class ConfigurationsController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Configurations. (NO SE USA EN NINGÚN LADO)
     */
    public function index()
    {
        try
        {
            $configurations = Configuration::all();

            return $configurations;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: configurations. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de configuration.
     */
    public function show(Configuration $configuration, Request $request)
    {
        $configuration->load( 'appointmentEmailTemplate', 'appointmentSmsTemplate', 'resultsEmailAutoTemplate', 'resultsEmailRefererTemplate', 'resultsSmsTemplate', 'mammographyEmailTemplate', 'mammographySmsTemplate', 'resultsEmailTemplate','pollsEmailTemplate' );
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'configuration', 'id' => $configuration->id]), $request->all()['user_id']);

        return $configuration;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para editar una instancia de Configuration.
     */
    public function edit(Request $request, Configuration $configuration)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'appointment_sms_notification' => 'required|numeric|min:1',
                //'birthday_expression' => 'required|max:45',
                'birad_1' => 'required|numeric|min:1',
                'birad_2' => 'required|numeric|min:1',
                'birad_3' => 'required|numeric|min:1',
                'birad_4' => 'required|numeric|min:1',
                'birad_5' => 'required|numeric|min:1',
                'birad_6' => 'required|numeric|min:1',
                'birad_7' => 'required|numeric|min:1',
                'birad_8' => 'required|numeric|min:1',
                'birad_9' => 'required|numeric|min:1',                
                'mammography_expression' => 'required|max:45',
                'mammography_reminder' => 'required|numeric|min:1',
                'appointment_sms_expression' => 'required|max:45',
                'appointment_sms_template_id' => 'required_with:appointment_sms', 
                'appointment_email_template_id' => 'required_with:appointment_email',
                'results_sms_template_id' => 'required_with:results_sms',
                'results_email_auto_template_id' => 'required_with:results_email_auto',
                'results_email_referer_template_id' => 'required_with:results_email_auto',
                'mammography_email_template_id' => 'required_with:mammography_email',
                'mammography_sms_template_id' => 'required_with:mammography_sms',
            ]);

            $original = new Configuration();
            foreach($configuration->getOriginal() as $key => $value) {
                $original->$key = $value;
            }

            $configuration->setZeros();

            try {
                $data = $request->all();
                $dataBirads = [];
                foreach ( $data as $key => $value ) {
                    if ( strpos( $key, 'birad_' ) !== false ) {
                        $dataBirads[] = [
                            'id'        => substr($key, strlen('birad_')),
                            'frecuency' => $value,
                        ];
                        unset($data[$key]);
                    }
                }
                
                if($configuration->update($data)) {
                
                    for ( $i=0; $i<count($dataBirads); $i++ ) {
                        $birad = \App\BiRad::find( $dataBirads[$i]['id'] );
                        $birad->update($dataBirads[$i]);
                    }

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'configuration', 'id' => $configuration->id, 'oldValue' => $original, 'newValue' => $configuration]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.configuration')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $configuration->id, 'section' => 'configuration', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.configuration')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: configurations. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }


            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $configuration]);
        }
        return $configuration;
    }
}
