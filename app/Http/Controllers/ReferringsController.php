<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Referring;
use App\User;
use Activity;
use Log;

use App\Http\Requests;

class ReferringsController extends Controller
{

    public function index( Request $request ) {
        try {
            if ( isset($request->all()['where']) ) {
                $where = $request->all()['where'];
                $referrings = Referring::where($where)->orderBy('last_name', 'asc')->get();
            } else {
                $referrings = Referring::orderBy('last_name', 'asc')->get();
            }

            return $referrings;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: referrings. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function show(Referring $referring, Request $request)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'referring', 'id' => $referring->id]), $request->all()['user_id']);
        
        return $referring;
    }

    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'administrative_ID' => 'required|unique:referrings|max:45',
                'email' => 'required|email|max:255|unique:referrings',
                'referring_institution' => 'required|max:250',
                'speciality' => 'required|max:250',
            ]);

            $referring = new Referring($request->all());

            try
            {
                if($referring->save())
                {
                    /**
                     * Log activity
                     */

                    //the index user_id to log was changed to user because the table already have a user_id field
                    Activity::log(trans('tracking.create', ['section' => 'referring', 'id' => $referring->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.referring')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    //the index user_id to log was changed to user because the table already have a user_id field
                    Activity::log(trans('tracking.attempt', ['section' => 'referring', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.referring')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: referrings. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $referring->id]);
        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    public function edit(Request $request, Referring $referring)
    {

        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'administrative_ID' => 'required|unique:referrings,administrative_ID,'.$referring->id.'|max:45',
                'email' => 'required|email|max:255|unique:referrings,email,'.$referring->id,
            ]);

            $original = new Referring();
            foreach($referring->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $referring->active = 0;

            try
            {
                if($referring->update($request->all()))
                {
                    /**
                     * Log activity
                     */

                    //the index user_id to log was changed to user because the table already have a user_id field
                    Activity::log(trans('tracking.edit', ['section' => 'referring', 'id' => $referring->id, 'oldValue' => $original, 'newValue' => $referring]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.referring')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    //the index user_id to log was changed to user because the table already have a user_id field
                    Activity::log(trans('tracking.attempt-edit', ['id' => $referring->id, 'section' => 'referring', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.referring')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: referrings. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $referring]);
        }
        $users = User::all();
        return response()->json(['users' => $users, 'referring' => $referring]);
    }

    public function delete(Request $request, Referring $referring)
    {
        try
        {
            if($referring->delete())
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $referring->id, 'section' => 'referring']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.referring')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $referring->id, 'section' => 'referring', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.referring')]));
                $request->session()->flash('class', 'alert alert-danger');
            }
            return response()->json(['code' => '200', 'message' => 'Deleted']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: referrings. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function active(Request $request, Referring $referring)
    {
        try
        {
            $original = new Referring();
            foreach($referring->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $referring->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'referrings', 'id' => $referring->id, 'oldValue' => $original, 'newValue' => $referring, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: referrings. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $referring]);
    }
}
