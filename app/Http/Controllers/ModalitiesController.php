<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modality;
use Activity;
use Log;

use App\Http\Requests;

class ModalitiesController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Modalities.
     */
    public function index(Request $request)
    {
        try
        {
            if ( isset($request->all()['where']) ) {
                $where = $request->all()['where'];
                $modalities = Modality::where($where)->get();
            } else {
                $modalities = Modality::all();
            }
            return $modalities;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: modalities. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
        }

    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Agregar una nueva instancia de Modality.
     */
    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required|max:10',
                'parent_id' => 'required',
            ]);

            $modality = new Modality($request->all());

            if($modality->parent_id == 0)
            {
                $modality->level = 0;
            }
            else
            {
                $modality->level = Modality::find($modality->parent_id)->level + 1;
            }

            try
            {
                if($modality->save())
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'modality', 'id' => $modality->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.modality')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'modality', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.modality')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: modalities. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $modality->id]);
        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de Modality
     */
    public function show(Modality $modality, Request $request)
    {

        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'modality', 'id' => $modality->id]), $request->all()['user_id']);

        $modality->load('parentModality');
        $modality->load('childModalities');

        return $modality;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Editar una instancia de Modality.
     */
    public function edit(Request $request, Modality $modality)
    {

        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required|max:10',
            ]);

            $original = new Modality();
            foreach($modality->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            // se coloca el active del $modality como 0 de manera predeterminada
            $modality->active = 0;

            $original->load('parentModality');
            $original->load('childModalities');

            if($modality->parent_id == 0)
            {
                $modality->level = 0;
            }
            else
            {
                $modality->level = Modality::find($modality->parent_id)->level + 1;
            }

            try
            {

                if($modality->update($request->all()))
                {
                    $modality->load('parentModality');
                    $modality->load('childModalities');

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'modality', 'id' => $modality->id, 'oldValue' => $original, 'newValue' => $modality]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.modality')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $modality->id, 'section' => 'modality', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.modality')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: modalities. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated']);
        }

        $modalities = Modality::where('id', '!=', $modality->id)->get();
        return response()->json(['modality' => $modality, 'modalities' => $modalities]);
    }

    public function delete(Request $request, Modality $modality)
    {
        try
        {
            if($modality->delete())
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $modality->id, 'section' => 'modality']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.modality')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $modality->id, 'section' => 'modality', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.modality')]));
                $request->session()->flash('class', 'alert alert-danger');
            }
            return response()->json(['code' => '200', 'message' => 'Deleted']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: modalities. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
        }
    }


    public function active(Request $request, Modality $modality)
    {
        try
        {
            $original = new Modality();
            foreach($modality->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $modality->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'procedures', 'id' => $modality->id, 'oldValue' => $original, 'newValue' => $modality, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: procedures. Action: active');
            //return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $modality]);
    }

}
