<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\RequestedProcedure;
use Activity;
use Log;
use DataSourceResult;

class TranscriberController extends Controller
{
    public function index(Request $request)
    {
        try
        {
            $host = config('database.connections.mysql.host');
            $dbname = config('database.connections.mysql.database');
            $conn = config('database.default');
            $username = config('database.connections.mysql.username');
            $password = config('database.connections.mysql.password');

            $DbHandler = new DataSourceResult("$conn:host=$host;dbname=$dbname", $username, $password);

            $req = $request->all();

            $data = (object) $req['data'];

            if(isset($data->filter))
            {
                $data->filter = (object) $data->filter;
                foreach ($data->filter->filters as $key => $filter)
                {
                    $data->filter->filters[$key] = (object) $filter;
                }
            }

            if(isset($data->sort))
            {
                foreach ($data->sort as $key => $sortArray)
                {
                    $data->sort[$key] = (object) $sortArray;
                }
            }

            $fields = ['isUrgent','rejectUserID','rejectUserName','patientTypeIcon','patientType','dictationDate','serviceRequestID','orderID','procedureDescription','patientName', 'radiologistUserID', 'radiologistUserName','institutionId'];
            $fields['patientID'] = ['type' => 'string'];    //This field needs a data type because with some values (14717687-2) it gets converted to a date
            $result = $DbHandler->read('transcribeIndexView', $fields, $data);

            return response()->json(['code' => '200', 'data' => $result]);   
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/transcriber/transcriber.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Transcriber. Action: index');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    public function show(Request $request, RequestedProcedure $requestedProcedure)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'transcriber', 'id' => $requestedProcedure->id]), $request->all()['user_id']);

        $requestedProcedure->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);

        $requestedProcedure->serviceRequest->load(['requestDocuments', 'source', 'patientType', 'requestStatus', 'smokingStatus', 'answer', 'pregnancyStatus', 'referring']);

        $requestedProcedure->procedure->load(['templates']);

        return $requestedProcedure;
    }

    public function edit(Request $request, RequestedProcedure $requestedProcedure)
    {
        try
        {

            $original = new RequestedProcedure();
            foreach($requestedProcedure->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $requestedProcedure->update($request->all());

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'transcriber', 'id' => $requestedProcedure->id, 'oldValue' => $original, 'newValue' => $requestedProcedure, 'action' => 'edit']), $request->all()['user_id']);


        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/transcriber/transcriber.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: transcriber. Action: edit');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }

        $requestedProcedure->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);
        $original->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);

        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $requestedProcedure]);
    }
}
