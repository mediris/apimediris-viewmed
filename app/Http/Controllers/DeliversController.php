<?php

namespace App\Http\Controllers;

use App\RequestedProcedure;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Deliver;
use Activity;
use Log;

class DeliversController extends Controller
{
    public function index(Request $request)
    {
        try
        {
            $delivers = Deliver::all();
            $delivers->load('requestedProcedures');

            return $delivers;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/delivers/delivers.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: delivers. Action: index');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'requested_procedure_id' => 'required',
                'date' => 'required',
                'receptor_id' => 'required',
                'receptor_name' => 'required',
                'responsable_id' => 'required',
                'responsable_name' => 'required'
            ]);

            $deliver = new Deliver($request->all());

            try
            {
                $deliver->save();

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.create', ['section' => 'delivers', 'id' => $deliver->id]), $request->all()['user_id']);

                return response()->json(['code' => '201', 'message' => 'Created', 'id' => $deliver->id]);
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/delivers/delivers.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: delivers. Action: add');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }
        }
    }
}
