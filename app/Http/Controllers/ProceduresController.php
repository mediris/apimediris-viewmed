<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;
use App\Procedure;
use App\Template;
use App\Order;
use App\Step;
use App\Http\Requests;
use Activity;
use Log;
use DB;

class ProceduresController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Procedures.
     */
    public function index(Request $request)
    {
        try
        {
            /* $procedures = Procedure::orderBy('description', 'asc')->get();
            $procedures->load(['templates', 'steps', 'rooms']);
            return $procedures;*/

            if ( isset($request->all()['where']) ) {
                $where = $request->all()['where'];
                $procedures = Procedure::where($where)->orderBy('description', 'asc')->get();
            } else {
                $procedures = Procedure::orderBy('description', 'asc')->get();
            }
            $procedures->load(['templates', 'steps', 'rooms', 'modality']);
            return $procedures;

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: procedures. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function indexEquipment(Request $request)
    {
        try
        {
            /* $procedures = Procedure::orderBy('description', 'asc')->get();
            $procedures->load(['templates', 'steps', 'rooms']);
            return $procedures;*/

            $procedures=DB::table('procedures')
                ->join('procedure_step','procedure_step.procedure_id','=','procedures.id')
                ->join('equipment_step','equipment_step.step_id','=','procedure_step.step_id')
                ->where('procedures.active','=','1')
                ->select('procedures.*')
                ->groupBy('procedures.id')
                ->get()
            ;
                //$procedures = Procedure::where('id','=',30)->orderBy('description', 'asc')->get();

            //$procedures->load(['templates', 'steps', 'rooms', 'modality']);
            return $procedures;

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: procedures. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instacia de Procedure.
     */
    public function show(Procedure $procedure, Request $request)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'procedure', 'id' => $procedure->id]), $request->all()['user_id']);

        $procedure->load(['orders', 'templates', 'steps', 'modality']);

        return $procedure;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Crear una nueva instancia de Procedure.
     */
    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:50',
                'radiologist_fee' => 'required|numeric|min:0',
                'technician_fee' => 'required|numeric|min:0',
                'transcriptor_fee' => 'required|numeric|min:0',
                'administrative_ID' => 'required|max:45',
                'modality_id'       => 'required'
            ]);
            
            $procedure = new Procedure($request->all());

            $steps = $request->input('steps');



            try
            {
                DB::transaction(function() use($request, $procedure, $steps)
                {
                    $procedure->save();

                    $procedure->templates()->attach($request->input('templates'));

                    if($steps)
                    {
                        foreach($steps as $data)
                        {


                            if(!isset($data['id']))
                            {
                                $step = new Step($data);
                                $step->save();
                            }
                            else
                            {
                                $step = Step::find($data['id']);
                                $step->update($data);
                                $step->consumables()->detach();
                                $step->equipment()->detach();
                            }

                            isset($data['consumables']) ? $step->consumables()->attach($data['consumables']) : '';
                            isset($data['equipment']) ? $step->equipment()->attach($data['equipment']) : '';

                            $procedure->steps()->attach($step->id, ['order' => $data['order']]);
                        }
                    }

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'procedure', 'id' => $procedure->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.procedure')]));
                    $request->session()->flash('class', 'alert alert-success');

                });

            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: procedures. Action: add');


                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $procedure->id]);
        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Editar una nueva instancia de Procedure.
     */
    public function edit(Request $request, Procedure $procedure)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:50',
                'radiologist_fee' => 'required|numeric|min:0',
                'technician_fee' => 'required|numeric|min:0',
                'transcriptor_fee' => 'required|numeric|min:0',
                'administrative_ID' => 'required|max:45'
            ]);

            $original = new Procedure();
            foreach($procedure->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            try
            {
                $steps = $request->input('steps');

                DB::transaction(function() use($request, $procedure, $steps, $original)
                {

                    $procedure->setZeros();

                    $procedure->steps()->detach();


                    $procedure->templates()->detach();
                    $procedure->templates()->attach($request->input('templates'));


                    $procedure->update($request->all());

                    if($steps)
                    {
                        foreach($steps as $data)
                        {

                            if(!isset($data['id']))
                            {
                                $step = new Step($data);
                                $step->save();
                            }
                            else
                            {
                                $step = Step::find($data['id']);
                                $step->update($data);
                                $step->consumables()->detach();
                                $step->equipment()->detach();
                            }

                            isset($data['consumables']) ? $step->consumables()->attach($data['consumables']) : '';
                            isset($data['equipment']) ? $step->equipment()->attach($data['equipment']) : '';

                            $procedure->steps()->attach($step->id, ['order' => $data['order']]);
                        }
                    }


                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'procedure', 'id' => $procedure->id, 'oldValue' => $original, 'newValue' => $procedure]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.procedure')]));
                    $request->session()->flash('class', 'alert alert-success');

                });


            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: procedures. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $procedure]);
        }
        $orders = Order::all();
        $templates = Template:: all();
        $steps = Step::all();
        return response()->json(['orders' => $orders, 'templates' => $templates, 'steps' => $steps]);
    }

    public function delete(Request $request, Procedure $procedure)
    {
        try
        {
            if($procedure->delete())
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $procedure->id, 'section' => 'procedure']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.procedure')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $procedure->id, 'section' => 'institution', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.procedure')]));
                $request->session()->flash('class', 'alert alert-danger');
            }

            return response()->json(['code' => '200', 'message' => 'Deleted']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: procedures. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Rooms asociados a la instancia de Procedure.
     */
    public function rooms(Request $request, Procedure $procedure)
    {
        return $procedure->rooms()->get();
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Cambiar el estado active de una instancia de Procedure.
     */
    public function active(Request $request, Procedure $procedure)
    {
        try
        {
            $original = new Procedure();
            foreach($procedure->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $procedure->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'procedures', 'id' => $procedure->id, 'oldValue' => $original, 'newValue' => $procedure, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: procedures. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $procedure]);
    }
}
