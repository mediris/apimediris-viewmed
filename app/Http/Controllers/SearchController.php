<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Worklist;
use Activity;
use Log;
use DB;
use DataSourceResult;
use App\RequestedProcedure;
use App\ServiceRequest;
use App\Search;

class SearchController extends Controller
{
    public function __construct()
    {
        $host = config('database.connections.mysql.host');
        $dbname = config('database.connections.mysql.database');
        $conn = config('database.default');
        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');
        $collate = config('database.connections.mysql.collation');

        $this->DbHandler = new DataSourceResult("$conn:host=$host;dbname=$dbname", $username, $password, array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8' COLLATE '{$collate}'"));
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra
     * @objetivo: Retornar una colección en formato Json de RequestedProcedures en base a la vista searchIndexView.
     */
    public function index(Request $request)
    {
        try
        {

            $fields = [ 
                'patientTypeIcon',
                'patientType',
                'orderStatus',
                'orderID',
                'orderStatusID',
                'orderIdentificationID',
                'patientName',
                'patientIdentificationID',
                'patientID',
                'procedureDescription',
                'referring',
                'approveUserName',
                'reportText',
                'radiologistUserName',
                'radiologistUserID',
                'admissionDate',
                'technicianEndDate',
                'dictationDate',
                'transcriptionDate',
                'approvalDate',
                'suspensionDate',
                'biradID',
                'serviceRequestID',
                'institutionId',
                'patientSex',
                'cellPhone',
                'modalities',
                'teachingFileText',
                'categoryID',
                'subCategoryID',
                'biopsyResult',
                'institutionId'
            ];

            
            $req = $request->all();

            $data = (object) $req['data'];

            if(isset($data->filter))
            {
                $data->filter = (object) $data->filter;
                foreach ($data->filter->filters as $key => $filter)
                {
                    $data->filter->filters[$key] = (object) $filter;
                }
            }

            if(isset($data->sort)) {
                foreach ($data->sort as $key => $sortArray) {
                    $data->sort[$key] = (object) $sortArray;
                }
            }
            
            $result = $this->DbHandler->read('searchIndexView', $fields, $data);
            
            return response()->json(['code' => '200', 'data' => $result ]);

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/search/search.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: search. Action: index');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra
     * @objetivo: Retornar una instancia de RequestedProcedure.
     */
    public function show(Request $request, RequestedProcedure $requestedProcedure)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'requestedProcedures', 'id' => $requestedProcedure->id]), $request->all()['user_id']);
        //$serviceRequest = ServiceRequest::find($requestedProcedure->service_request_id);
        
        $requestedProcedure->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);
        
        return $requestedProcedure;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra
     * @objetivo: Editar una instancia de RequestedProcedure.
     */
    public function edit(Request $request, RequestedProcedure $requestedProcedure, ServiceRequest $serviceRequest)
    {

        if($request->isMethod('post'))
        {
            // return response()->json(['code' => '200', 'data' => $request->all()['data'], 'rp' => $requestedProcedure]);

            $originalRequestedProcedure = new RequestedProcedure();

            foreach ($requestedProcedure->getOriginal() as $key => $value) {

                $originalRequestedProcedure->$key = $value;

            }

            $originalServiceRequest = new ServiceRequest();

            foreach ($serviceRequest->getOriginal() as $key => $value) {

                $originalServiceRequest->$key = $value;

            }

            try
            {

                DB::transaction(function() use($request, $requestedProcedure, $originalRequestedProcedure, $serviceRequest, $originalServiceRequest)
                {

                    $data = $request->all()['data'];
                    
                    if($status = $requestedProcedure->isRequestedProcedureStatusUpdated($data['requestedProcedure'])) {

                        $requestedProcedure->updateStatus($status);

                    }

                    $requestedProcedure->update($data['requestedProcedure']);
                    $serviceRequest->update($data['serviceRequest']);


                    $serviceRequest->load('requestedProcedures');
                    $originalServiceRequest->load('requestedProcedures');

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'serviceRequest', 'id' => $serviceRequest->id, 'oldValue' => $originalServiceRequest, 'newValue' => $serviceRequest]), $request->all()['user_id']);
                });
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/requestedProcedures/requestedProcedures.log');
                Log::alert('File: ' . $e->getFile() . ' Error code: ' . $e->getCode() . ' Error Line: ' . $e->getLine() . ' Error message: '. $e->getMessage() . ' Section: requestedProcedures. Action: edit');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $requestedProcedure]);
        }
    }
}
