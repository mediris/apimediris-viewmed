<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StepRequest;
use App\Step;
use App\Procedure;
use App\Http\Requests;
use Activity;
use Log;

class StepsController extends Controller
{
    public function index()
    {
        try
        {

            $steps = Step::orderBy('description', 'asc')->get();
            return $steps;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: steps. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function show(Step $step, Request $request)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'step', 'id' => $step->id]), $request->all()['user_id']);

        $step->load(['procedures', 'equipment', 'consumables']);
        return $step;
    }

    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:25',
                'indications' => 'required',
                'administrative_ID' => 'required|max:45',
            ]);

            $step = new Step($request->all());

            try
            {
                if($step->save())
                {
                    $step->equipment()->attach($request->input('equipment'));
                    $step->consumables()->attach($request->input('consumables'));
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'step', 'id' => $step->id]), $request->all()['user_id']);


                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.step')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'step', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.step')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: steps. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $step->id]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    public function edit(Request $request, Step $step)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:25',
                'indications' => 'required',
                'administrative_ID' => 'required|max:45',
            ]);

            $original = new Step();
            foreach($step->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $step->equipment()->detach();
            $step->consumables()->detach();
            $step->equipment()->attach($request->input('equipment'));
            $step->consumables()->attach($request->input('consumables'));
            $step->active = 0;

            try
            {
                if($step->update($request->all()))
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'step', 'id' => $step->id, 'oldValue' => $original, 'newValue' => $step]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.step')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $step->id, 'section' => 'step', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.step')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: steps. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $step, 'id' => $step->id]);
        }

        $procedures = Procedure::all();
        return response()->json(['procedures' => $procedures, 'steps' => $step]);
    }

    public function delete(Request $request, Step $step)
    {
        try
        {
            if($step->delete())
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $step->id, 'section' => 'step']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.step')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $step->id, 'section' => 'step', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.step')]));
                $request->session()->flash('class', 'alert alert-danger');
            }
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: steps. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }

        return response()->json(['code' => '200', 'message' => 'Deleted']);
    }

    public function active(Request $request, Step $step)
    {
        try
        {
            $original = new Step();
            foreach($step->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $step->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'steps', 'id' => $step->id, 'oldValue' => $original, 'newValue' => $step, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: steps. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $step]);
    }
}
