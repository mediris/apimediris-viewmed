<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/',  ['middleware' => 'auth', function () {
    return view('welcome');
}]);



Route::auth();

Route::get('/home', 'HomeController@index');


/**
 * IMPORTANTE!!
 * Rutas utilizadas para ingresar al api
 * El middleware 'auth:api' sólo permite el acceso a las rutas si se envia en la trama un campo llamado 'api_token' con algún valor válido que se encuentre almacenado en la tabla users
 */

Route::group(['prefix' => 'api/v1', 'middleware' => 'auth:api'], function(){

//Route::group(['prefix' => 'api/v1'], function(){


    Route::get('/', function(){
        return view('home');
    });

    /**
     * Route to check unique
     */

    Route::post('/unique', ['as' => 'unique', 'uses' => 'Controller@unique']);

 /**
     * Routes of Fix
     */

    Route::group(['prefix' => 'fix'], function(){

        Route::post('/edit/{servicerequests}', ['as' => 'fix', 'uses' => 'ServiceRequestsController@corregir']);
        Route::get('/edit/{servicerequests}', ['as' => 'fix', 'uses' => 'ServiceRequestsController@corregir']);
        

    });

    /**
     * Routes of Users
     */

    Route::group(['prefix' => 'users'], function(){

        Route::get('/users', ['as' => 'users', 'uses' => 'UsersController@index']);
        Route::get('/add', ['as' => 'users.add', 'uses' => 'UsersController@add']);
        Route::post('/add', ['as' => 'users.add', 'uses' => 'UsersController@add']);
        Route::get('/show/{user}', ['as' => 'users.show', 'uses' => 'UsersController@show']);
        Route::get('/edit/{user}', ['as' => 'users.edit', 'uses' => 'UsersController@edit']);
        Route::post('/edit/{user}', ['as' => 'users.edit', 'uses' => 'UsersController@edit']);
        Route::get('/delete/{user}', ['as' => 'users.delete', 'uses' => 'UsersController@delete']);
        Route::post('/change_password/{user}', ['as' => 'users.password', 'uses' => 'UsersController@change_password']);

    });

    /**
     * Routes of Divisions
     */

    Route::group(['prefix' => 'divisions'], function(){

        Route::get('/', ['as' => 'divisions', 'uses' => 'DivisionsController@index']);
        Route::post('/', ['as' => 'divisions', 'uses' => 'DivisionsController@index']);
        Route::get('/add', ['as' => 'divisions.add', 'uses' => 'DivisionsController@add']);
        Route::post('/add', ['as' => 'divisions.add', 'uses' => 'DivisionsController@add']);
        Route::get('/show/{division}', ['as' => 'divisions.show', 'uses' => 'DivisionsController@show']);
        Route::post('/show/{division}', ['as' => 'divisions.show', 'uses' => 'DivisionsController@show']);
        Route::get('/edit/{division}', ['as' => 'divisions.edit', 'uses' => 'DivisionsController@edit']);
        Route::post('/edit/{division}', ['as' => 'divisions.edit', 'uses' => 'DivisionsController@edit']);
        /*Route::get('/delete/{division}', ['as' => 'divisions.delete', 'uses' => 'DivisionsController@delete']);
        Route::post('/delete/{division}', ['as' => 'divisions.delete', 'uses' => 'DivisionsController@delete']);*/
        Route::post('/active/{division}', ['as' => 'divisions.active', 'uses' => 'DivisionsController@active']);

    });

    /**
     * Routes of Rooms
     */

    Route::group(['prefix' => 'rooms'], function(){

        Route::get('/', ['as' => 'rooms', 'uses' => 'RoomsController@index']);
        Route::post('/', ['as' => 'rooms', 'uses' => 'RoomsController@index']);
        Route::get('/add', ['as' => 'rooms.add', 'uses' => 'RoomsController@add']);
        Route::post('/add', ['as' => 'rooms.add', 'uses' => 'RoomsController@add']);
        Route::get('/show/{room}', ['as' => 'rooms.show', 'uses' => 'RoomsController@show']);
        Route::post('/show/{room}', ['as' => 'rooms.show', 'uses' => 'RoomsController@show']);
        Route::get('/edit/{room}', ['as' => 'rooms.edit', 'uses' => 'RoomsController@edit']);
        Route::post('/edit/{room}', ['as' => 'rooms.edit', 'uses' => 'RoomsController@edit']);
        /*Route::get('/delete/{room}', ['as' => 'rooms.delete', 'uses' => 'RoomsController@delete']);
        Route::post('/delete/{room}', ['as' => 'rooms.delete', 'uses' => 'RoomsController@delete']);*/
        Route::post('/active/{room}', ['as' => 'rooms.active', 'uses' => 'RoomsController@active']);

    });


    /**
     * Routes of Modalities
     */

    Route::group(['prefix' => 'modalities'], function(){

        Route::get('/', ['as' => 'modalities', 'uses' => 'ModalitiesController@index']);
        Route::post('/', ['as' => 'modalities', 'uses' => 'ModalitiesController@index']);
        Route::get('/add', ['as' => 'modalities.add', 'uses' => 'ModalitiesController@add']);
        Route::post('/add', ['as' => 'modalities.add', 'uses' => 'ModalitiesController@add']);
        Route::post('/show/{modality}', ['as' => 'modalities.show', 'uses' => 'ModalitiesController@show']);
        Route::get('/edit/{modality}', ['as' => 'modalities.edit', 'uses' => 'ModalitiesController@edit']);
        Route::post('/edit/{modality}', ['as' => 'modalities.edit', 'uses' => 'ModalitiesController@edit']);
        Route::get('/delete/{modality}', ['as' => 'modalities.delete', 'uses' => 'ModalitiesController@delete']);
        Route::post('/active/{modality}', ['as' => 'modalities.active', 'uses' => 'ModalitiesController@active']);

    });

    /**
     * Routes of Equipment
     */

    Route::group(['prefix' => 'equipment'], function() {
        Route::get ('/',                            ['as' => 'equipment', 'uses' => 'EquipmentController@index']);
        Route::post('/',                            ['as' => 'equipment', 'uses' => 'EquipmentController@index']);
        Route::get ('/add',                         ['as' => 'equipment.add', 'uses' => 'EquipmentController@add']);
        Route::post('/add',                         ['as' => 'equipmen.add', 'uses' => 'EquipmentController@add']);
        Route::get ('/show/{equipment}',            ['as' => 'equipment.show', 'uses' => 'EquipmentController@show']);
        Route::post('/show/{equipment}',            ['as' => 'equipment.show', 'uses' => 'EquipmentController@show']);
        Route::get ('/edit/{equipment}',            ['as' => 'equipment.edit', 'uses' => 'EquipmentController@edit']);
        Route::post('/edit/{equipment}',            ['as' => 'equipment.edit', 'uses' => 'EquipmentController@edit']);
        /*Route::get('/delete/{equipment}', ['as' => 'equipment.delete', 'uses' => 'EquipmentController@delete']);
        Route::post('/delete/{equipment}', ['as' => 'equipment.delete', 'uses' => 'EquipmentController@delete']);*/
        Route::post('/active/{equipment}',          ['as' => 'equipment.active', 'uses' => 'EquipmentController@active']);
        Route::post('showbyprocedure/{procedure}',  ['as' => 'equipment.showbyprocedure', 'uses' => 'EquipmentController@showbyprocedure']);
    });

    /**
     * Routes of Referrings
     */

    Route::group(['prefix' => 'referrings'], function(){

        Route::get('/', ['as' => 'referrings', 'uses' => 'ReferringsController@index']);
        Route::post('/', ['as' => 'referrings', 'uses' => 'ReferringsController@index']);
        Route::get('/add', ['as' => 'referrings.add', 'uses' => 'ReferringsController@add']);
        Route::post('/add', ['as' => 'referrings.add', 'uses' => 'ReferringsController@add']);
        Route::get('/show/{referring}', ['as' => 'referrings.show', 'uses' => 'ReferringsController@show']);
        Route::post('/show/{referring}', ['as' => 'referrings.show', 'uses' => 'ReferringsController@show']);
        Route::get('/edit/{referring}', ['as' => 'referrings.edit', 'uses' => 'ReferringsController@edit']);
        Route::post('/edit/{referring}', ['as' => 'referrings.edit', 'uses' => 'ReferringsController@edit']);
        /*Route::get('/delete/{referring}', ['as' => 'referrings.delete', 'uses' => 'ReferringsController@delete']);
        Route::post('/delete/{referring}', ['as' => 'referrings.delete', 'uses' => 'ReferringsController@delete']);*/
        Route::post('/active/{referring}', ['as' => 'referrings.active', 'uses' => 'ReferringsController@active']);

    });

    /**
     * Routes of Plates Sizes
     */

    Route::group(['prefix' => 'platessizes'], function(){

        Route::get('/', ['as' => 'platessizes', 'uses' => 'PlatesSizeController@index']);
        Route::post('/', ['as' => 'platessizes', 'uses' => 'PlatesSizeController@index']);
        Route::get('/add', ['as' => 'platessizes.add', 'uses' => 'PlatesSizeController@add']);
        Route::post('/add', ['as' => 'platessizes.add', 'uses' => 'PlatesSizeController@add']);
        Route::get('/show/{platessize}', ['as' => 'platessizes.show', 'uses' => 'PlatesSizeController@show']);
        Route::post('/show/{platessize}', ['as' => 'platessizes.show', 'uses' => 'PlatesSizeController@show']);
        Route::get('/edit/{platessize}', ['as' => 'platessizes.edit', 'uses' => 'PlatesSizeController@edit']);
        Route::post('/edit/{platessize}', ['as' => 'platessizes.edit', 'uses' => 'PlatesSizeController@edit']);
        /*Route::get('/delete/{platessize}', ['as' => 'platessizes.delete', 'uses' => 'PlatesSizeController@delete']);
        Route::post('/delete/{platessize}', ['as' => 'platessizes.delete', 'uses' => 'PlatesSizeController@delete']);*/
        Route::post('/active/{platessize}', ['as' => 'platessizes.active', 'uses' => 'PlatesSizeController@active']);

    });

    /**
     * Routes of Configurations
     */

    Route::group(['prefix' => 'configurations'], function(){

        Route::get('/', ['as' => 'configurations', 'uses' => 'ConfigurationsController@index']);
        Route::get('/show/{configuration}', ['as' => 'configurations.show', 'uses' => 'ConfigurationsController@show']);
        Route::post('/show/{configuration}', ['as' => 'configurations.show', 'uses' => 'ConfigurationsController@show']);
        Route::get('/edit/{configuration}', ['as' => 'configurations.edit', 'uses' => 'ConfigurationsController@edit']);
        Route::post('/edit/{configuration}', ['as' => 'configurations.edit', 'uses' => 'ConfigurationsController@edit']);

    });

    /**
     * Routes of PatientTypes
     */

    Route::group(['prefix' => 'patienttypes'], function(){

        Route::get('/', ['as' => 'patientTypes', 'uses' => 'PatientTypesController@index']);
        Route::post('/', ['as' => 'patientTypes', 'uses' => 'PatientTypesController@index']);
        Route::post('/roots', ['as' => 'patientTypes.roots', 'uses' => 'PatientTypesController@roots']);
        Route::get('/add', ['as' => 'patientTypes.add', 'uses' => 'PatientTypesController@add']);
        Route::post('/add', ['as' => 'patientTypes.add', 'uses' => 'PatientTypesController@add']);
        Route::get('/show/{patientType}', ['as' => 'patientTypes.show', 'uses' => 'PatientTypesController@show']);
        Route::post('/show/{patientType}', ['as' => 'patientTypes.show', 'uses' => 'PatientTypesController@show']);
        Route::get('/edit/{patientType}', ['as' => 'patientTypes.edit', 'uses' => 'PatientTypesController@edit']);
        Route::post('/edit/{patientType}', ['as' => 'patientTypes.edit', 'uses' => 'PatientTypesController@edit']);
        /*Route::get('/delete/{patientType}',['as' => 'patientTypes.delete', 'uses' => 'PatientTypesController@delete']);
        Route::post('/delete/{patientType}',['as' => 'patientTypes.delete', 'uses' => 'PatientTypesController@delete']);*/
        Route::post('/parent/{patientType}',['as' => 'patientTypes.parent', 'uses' => 'PatientTypesController@parent']);
        Route::post('/childs/{patientType}',['as' => 'patientTypes.childs', 'uses' => 'PatientTypesController@childs']);
        Route::post('/active/{patientType}', ['as' => 'patientTypes.active', 'uses' => 'PatientTypesController@active']);

    });

    /**
     * Routes of Sources
     */

    Route::group(['prefix' => 'sources'], function(){

        Route::get('/', ['as' => 'sources', 'uses' => 'SourcesController@index']);
        Route::post('/', ['as' => 'sources', 'uses' => 'SourcesController@index']);
        Route::get('/add', ['as' => 'sources.add', 'uses' => 'SourcesController@add']);
        Route::post('/add', ['as' => 'sources.add', 'uses' => 'SourcesController@add']);
        Route::get('/show/{source}', ['as' => 'sources.show', 'uses' => 'SourcesController@show']);
        Route::post('/show/{source}', ['as' => 'sources.show', 'uses' => 'SourcesController@show']);
        Route::get('/edit/{source}', ['as' => 'sources.edit', 'uses' => 'SourcesController@edit']);
        Route::post('/edit/{source}', ['as' => 'sources.edit', 'uses' => 'SourcesController@edit']);
        /*Route::get('/delete/{source}', ['as' => 'sources.delete', 'uses' => 'SourcesController@delete']);
        Route::post('/delete/{source}', ['as' => 'sources.delete', 'uses' => 'SourcesController@delete']);*/
        Route::post('/active/{source}', ['as' => 'sources.active', 'uses' => 'SourcesController@active']);

    });

    /**
     * Routes of Procedures
     */

    Route::group(['prefix' => 'procedures'], function(){

        Route::get('/', ['as' => 'procedures', 'uses' => 'ProceduresController@index']);
        Route::post('/', ['as' => 'procedures', 'uses' => 'ProceduresController@index']);
        Route::get('/add', ['as' => 'procedures.add', 'uses' => 'ProceduresController@add']);
        Route::post('/equipments', ['as' => 'procedures.equipments', 'uses' => 'ProceduresController@indexEquipment']);
        Route::post('/add', ['as' => 'procedures.add', 'uses' => 'ProceduresController@add']);
        Route::get('/show/{procedure}', ['as' => 'procedures.show', 'uses' => 'ProceduresController@show']);
        Route::post('/show/{procedure}', ['as' => 'procedures.show', 'uses' => 'ProceduresController@show']);
        Route::get('/edit/{procedure}', ['as' => 'procedures.edit', 'uses' => 'ProceduresController@edit']);
        Route::post('/edit/{procedure}', ['as' => 'procedures.edit', 'uses' => 'ProceduresController@edit']);
        /*Route::get('/delete/{procedure}', ['as' => 'procedures.delete', 'uses' => 'ProceduresController@delete']);
        Route::post('/delete/{procedure}', ['as' => 'procedures.delete', 'uses' => 'ProceduresController@delete']);*/
        Route::post('/active/{procedure}', ['as' => 'procedures.active', 'uses' => 'ProceduresController@active']);
        Route::post('/modalities/{procedure}', ['as' => 'procedures.modalities', 'uses' => 'ProceduresController@modalities']);
        Route::post('/rooms/{procedure}', ['as' => 'procedures.rooms', 'uses' => 'ProceduresController@rooms']);

    });

    /**
     * Routes of Templates
     */

    Route::group(['prefix' => 'templates'], function(){

        Route::get('/', ['as' => 'templates', 'uses' => 'TemplatesController@index']);
        Route::post('/', ['as' => 'templates', 'uses' => 'TemplatesController@index']);
        Route::get('/add', ['as' => 'templates.add', 'uses' => 'TemplatesController@add']);
        Route::post('/add', ['as' => 'templates.add', 'uses' => 'TemplatesController@add']);
        Route::get('/show/{template}', ['as' => 'templates.show', 'uses' => 'TemplatesController@show']);
        Route::post('/show/{template}', ['as' => 'templates.show', 'uses' => 'TemplatesController@show']);
        Route::get('/edit/{template}', ['as' => 'templates.edit', 'uses' => 'TemplatesController@edit']);
        Route::post('/edit/{template}', ['as' => 'templates.edit', 'uses' => 'TemplatesController@edit']);
        /*Route::get('/delete/{template}', ['as' => 'templates.delete', 'uses' => 'TemplatesController@delete']);
        Route::post('/delete/{template}', ['as' => 'templates.delete', 'uses' => 'TemplatesController@delete']);*/
        Route::post('/active/{template}', ['as' => 'templates.active', 'uses' => 'TemplatesController@active']);

    });

    /**
     * Routes of Templates
     */

    Route::group(['prefix' => 'notification_templates'], function(){

        Route::get('/', ['as' => 'notification_templates', 'uses' => 'NotificationTemplatesController@index']);
        Route::post('/', ['as' => 'notification_templates', 'uses' => 'NotificationTemplatesController@index']);
        Route::post('/showactive', ['as' => 'notification_templates', 'uses' => 'NotificationTemplatesController@index']);
        Route::post('/add', ['as' => 'notification_templates.add', 'uses' => 'NotificationTemplatesController@add']);
        Route::post('/show/{template}', ['as' => 'notification_templates.show', 'uses' => 'NotificationTemplatesController@show']);
        Route::post('/edit/{template}', ['as' => 'notification_templates.edit', 'uses' => 'NotificationTemplatesController@edit']);
        Route::post('/active/{template}', ['as' => 'notification_templates.active', 'uses' => 'NotificationTemplatesController@active']);

    });

    /**
     * Routes of Consumables
     */

    Route::group(['prefix' => 'consumables'], function(){

        Route::get('/', ['as' => 'consumables', 'uses' => 'ConsumablesController@index']);
        Route::post('/', ['as' => 'consumables', 'uses' => 'ConsumablesController@index']);
        Route::get('/add', ['as' => 'consumables.add', 'uses' => 'ConsumablesController@add']);
        Route::post('/add', ['as' => 'consumables.add', 'uses' => 'ConsumablesController@add']);
        Route::get('/show/{consumable}', ['as' => 'consumables.show', 'uses' => 'ConsumablesController@show']);
        Route::post('/show/{consumable}', ['as' => 'consumables.show', 'uses' => 'ConsumablesController@show']);
        Route::get('/edit/{consumable}', ['as' => 'consumables.edit', 'uses' => 'ConsumablesController@edit']);
        Route::post('/edit/{consumable}', ['as' => 'consumables.edit', 'uses' => 'ConsumablesController@edit']);
        /*Route::get('/delete/{consumable}', ['as' => 'consumables.delete', 'uses' => 'ConsumablesController@delete']);
        Route::post('/delete/{consumable}', ['as' => 'consumables.delete', 'uses' => 'ConsumablesController@delete']);*/
        Route::post('/active/{consumable}', ['as' => 'consumables.active', 'uses' => 'ConsumablesController@active']);

    });

    /**
     * Routes of Units
     */

    Route::group(['prefix' => 'units'], function(){

        Route::get('/', ['as' => 'units', 'uses' => 'UnitsController@index']);
        Route::post('/', ['as' => 'units', 'uses' => 'UnitsController@index']);
        Route::get('/add', ['as' => 'units.add', 'uses' => 'UnitsController@add']);
        Route::post('/add', ['as' => 'units.add', 'uses' => 'UnitsController@add']);
        Route::get('/show/{unit}', ['as' => 'units.show', 'uses' => 'UnitsController@show']);
        Route::post('/show/{unit}', ['as' => 'units.show', 'uses' => 'UnitsController@show']);
        Route::get('/edit/{unit}', ['as' => 'units.edit', 'uses' => 'UnitsController@edit']);
        Route::post('/edit/{unit}', ['as' => 'units.edit', 'uses' => 'UnitsController@edit']);
        /*Route::get('/delete/{unit}', ['as' => 'units.delete', 'uses' => 'UnitsController@delete']);
        Route::post('/delete/{unit}', ['as' => 'units.delete', 'uses' => 'UnitsController@delete']);*/
        Route::post('/active/{unit}', ['as' => 'units.active', 'uses' => 'UnitsController@active']);

    });

    /**
     * Routes of Printers
     */

    Route::group(['prefix' => 'printers'], function(){

        Route::get('/', ['as' => 'printers', 'uses' => 'PrintersController@index']);
        Route::post('/', ['as' => 'printers', 'uses' => 'PrintersController@index']);
        Route::get('/add', ['as' => 'printers.add', 'uses' => 'PrintersController@add']);
        Route::post('/add', ['as' => 'printers.add', 'uses' => 'PrintersController@add']);
        Route::get('/show/{printer}', ['as' => 'printers.show', 'uses' => 'PrintersController@show']);
        Route::post('/show/{printer}', ['as' => 'printers.show', 'uses' => 'PrintersController@show']);
        Route::get('/edit/{printer}', ['as' => 'printers.edit', 'uses' => 'PrintersController@edit']);
        Route::post('/edit/{printer}', ['as' => 'printers.edit', 'uses' => 'PrintersController@edit']);
        /*Route::get('/delete/{printer}', ['as' => 'printers.delete', 'uses' => 'PrintersController@delete']);
        Route::post('/delete/{printer}', ['as' => 'printers.delete', 'uses' => 'PrintersController@delete']);*/
        Route::post('/active/{printer}', ['as' => 'printers.active', 'uses' => 'PrintersController@active']);

    });

    /**
     * Routes of Orders
     */

    Route::group(['prefix' => 'orders'], function(){

        Route::get('/', ['as' => 'orders', 'uses' => 'OrdersController@index']);
        Route::post('/', ['as' => 'orders', 'uses' => 'OrdersController@index']);
        Route::get('/add', ['as' => 'orders.add', 'uses' => 'OrdersController@add']);
        Route::post('/add', ['as' => 'orders.add', 'uses' => 'OrdersController@add']);
        Route::get('/show/{order}', ['as' => 'orders.show', 'uses' => 'OrdersController@show']);
        Route::post('/show/{order}', ['as' => 'orders.show', 'uses' => 'OrdersController@show']);
        Route::get('/edit/{order}', ['as' => 'orders.edit', 'uses' => 'OrdersController@edit']);
        Route::post('/edit/{order}', ['as' => 'orders.edit', 'uses' => 'OrdersController@edit']);
        /*Route::get('/delete/{order}', ['as' => 'orders.delete', 'uses' => 'OrdersController@delete']);
        Route::post('/delete/{order}', ['as' => 'orders.delete', 'uses' => 'OrdersController@delete']);*/
        Route::post('/active/{order}', ['as' => 'orders.active', 'uses' => 'OrdersController@active']);

    });

    /**
     * Routes of Suspension Reasons (Suspension list for Technical)
     */
    Route::group(['prefix' => 'suspend_reasons'], function(){

        Route::get('/'                          , ['as' => 'suspend_reasons'             , 'uses' => 'SuspendReasonsController@index']);
        Route::post('/'                         , ['as' => 'suspend_reasons'             , 'uses' => 'SuspendReasonsController@index']);
        Route::post('/showactive'               , ['as' => 'suspend_reasons.showactive'  , 'uses' => 'SuspendReasonsController@index']);
        Route::post('/add'                      , ['as' => 'suspend_reasons.add'         , 'uses' => 'SuspendReasonsController@add']);
        Route::post('/show/{suspend_reason}'     , ['as' => 'suspend_reasons.show'        , 'uses' => 'SuspendReasonsController@show']);
        Route::post('/edit/{suspend_reason}'     , ['as' => 'suspend_reasons.edit'        , 'uses' => 'SuspendReasonsController@edit']);
        Route::post('/active/{suspend_reason}'   , ['as' => 'suspend_reasons.active'      , 'uses' => 'SuspendReasonsController@active']);

    });

    
    /**
     * Routes of Steps
     */
    Route::group(['prefix' => 'steps'], function(){

        Route::get('/', ['as' => 'steps', 'uses' => 'StepsController@index']);
        Route::post('/', ['as' => 'steps', 'uses' => 'StepsController@index']);
        Route::get('/add', ['as' => 'steps.add', 'uses' => 'StepsController@add']);
        Route::post('/add', ['as' => 'steps.add', 'uses' => 'StepsController@add']);
        Route::get('/show/{step}', ['as' => 'steps.show', 'uses' => 'StepsController@show']);
        Route::post('/show/{step}', ['as' => 'steps.show', 'uses' => 'StepsController@show']);
        Route::get('/edit/{step}', ['as' => 'steps.edit', 'uses' => 'StepsController@edit']);
        Route::post('/edit/{step}', ['as' => 'steps.edit', 'uses' => 'StepsController@edit']);
        /*Route::get('/delete/{step}', ['as' => 'steps.delete', 'uses' => 'StepsController@delete']);
        Route::post('/delete/{step}', ['as' => 'steps.delete', 'uses' => 'StepsController@delete']);*/
        Route::post('/active/{step}', ['as' => 'steps.active', 'uses' => 'StepsController@active']);

    });


    /**
     * Routes of Reception
     */
    Route::group(['prefix' => 'reception'], function(){

        Route::get('/', ['as' => 'reception', 'uses' => 'ServiceRequestsController@index']);
        Route::post('/', ['as' => 'reception', 'uses' => 'ServiceRequestsController@index']);

        Route::get('/add', ['as' => 'reception.add', 'uses' => 'ServiceRequestsController@add']);
        Route::post('/add', ['as' => 'reception.add', 'uses' => 'ServiceRequestsController@add']);

        Route::get('/show/{serviceRequest}', ['as' => 'reception.show', 'uses' => 'ServiceRequestsController@show']);
        Route::post('/show/{serviceRequest}', ['as' => 'reception.show', 'uses' => 'ServiceRequestsController@show']);

        Route::get('/edit/{serviceRequest}', ['as' => 'reception.edit', 'uses' => 'ServiceRequestsController@edit']);
        Route::post('/edit/{serviceRequest}', ['as' => 'reception.edit', 'uses' => 'ServiceRequestsController@edit']);

        Route::post('/total', ['as' => 'reception.total', 'uses' => 'ServiceRequestsController@total']);

        Route::post('/active/{serviceRequest}', ['as' => 'reception.active', 'uses' => 'ServiceRequestsController@active']);

        Route::post('/lock/{serviceRequest}', ['as' => 'reception.lock', 'uses' => 'ServiceRequestsController@lock']);
        Route::post('/lock/{serviceRequest}/{option}', ['as' => 'reception.lock.option', 'uses' => 'ServiceRequestsController@lock']);

        Route::post('/approve/{serviceRequest}', ['as' => 'reception.approve', 'uses' => 'ServiceRequestsController@approve']);

    });

    Route::group(['prefix' => 'servicerequestdocument'], function(){

        Route::post('/edit/{serviceRequest}', ['as' => 'servicerequestdocument.add', 'uses' => 'ServiceRequestsController@documentAdd']);
        Route::post('/show/{serviceRequestDocument}', ['as' => 'servicerequestdocument.add', 'uses' => 'ServiceRequestsController@documentShow']);

    });

    /**
     * Routes of RequestedProcedures
     */
    Route::group(['prefix' => 'requestedprocedures'], function(){

        Route::get ('/',                                    ['as' => 'requestedprocedures', 'uses' => 'RequestedProceduresController@index']);
        Route::post('/',                                    ['as' => 'requestedprocedures', 'uses' => 'RequestedProceduresController@index']);

        Route::get ('/add',                                 ['as' => 'requestedprocedures.add', 'uses' => 'RequestedProceduresController@add']);
        Route::post('/add',                                 ['as' => 'requestedprocedures.add', 'uses' => 'RequestedProceduresController@add']);

        Route::get ('/showbirad',                           ['as' => 'requestedprocedures.showbirad', 'uses' => 'RequestedProceduresController@showBirad']);
        Route::post('/showbirad',                           ['as' => 'requestedprocedures.showbirad', 'uses' => 'RequestedProceduresController@showBirad']);

        Route::get ('/show/{requestedProcedure}',           ['as' => 'requestedprocedures.show', 'uses' => 'RequestedProceduresController@show']);
        Route::post('/show/{requestedProcedure}',           ['as' => 'requestedprocedures.show', 'uses' => 'RequestedProceduresController@show']);

        Route::get('/edit/{requestedProcedure}',            ['as' => 'requestedprocedures.edit', 'uses' => 'RequestedProceduresController@edit']);
        Route::post('/edit/{requestedProcedure}',           ['as' => 'requestedprocedures.edit', 'uses' => 'RequestedProceduresController@edit']);

        Route::post('/active/{requestedProcedure}/{status}',['as' => 'requestedprocedures.active.requestedProcedure.status', 'uses' => 'RequestedProceduresController@active']);

        Route::post('/total/{status}',                      ['as' => 'requestedprocedures.total', 'uses' => 'RequestedProceduresController@total']);

        Route::post('/total-by-month/{status}',             ['as' => 'requestedprocedures.total.month', 'uses' => 'RequestedProceduresController@totalByMonth']);
        Route::post('/total-by-user/{status}/{user}',       ['as' => 'requestedprocedures.total.user', 'uses' => 'RequestedProceduresController@totalByUser']);

        Route::post('/lock/{requestedProcedure}/{role}',    ['as' => 'requestedprocedures.lock', 'uses' => 'RequestedProceduresController@lock']);
        Route::post('/lock/{requestedProcedure}/{role}/{option}', ['as' => 'requestedprocedures.lock', 'uses' => 'RequestedProceduresController@lock']);

        Route::post('/writeworklist/{requestedProcedure}',  ['as' => 'requestedprocedures.writeworklist', 'uses' => 'RequestedProceduresController@writeworklist']);

        Route::post('/setsi/{requestedProcedure}',          ['as' => 'requestedprocedures.setsi', 'uses' => 'RequestedProceduresController@setStudyInstance']);

    });

    /**
     * Routes of Appointments
     */
    Route::group(['prefix' => 'appointments'], function(){

        Route::get('/', ['as' => 'appointments', 'uses' => 'AppointmentsController@index']);
        Route::post('/', ['as' => 'appointments', 'uses' => 'AppointmentsController@index']);
        Route::get('/add', ['as' => 'appointments.add', 'uses' => 'AppointmentsController@add']);
        Route::post('/add', ['as' => 'appointments.add', 'uses' => 'AppointmentsController@add']);
        Route::get('/show/{appointment}', ['as' => 'appointments.show', 'uses' => 'AppointmentsController@show']);
        Route::post('/show/{appointment}', ['as' => 'appointments.show', 'uses' => 'AppointmentsController@show']);
        Route::post('/showcreated',          ['as' => 'appointments.showcreated', 'uses' => 'AppointmentsController@showCreated']);
        Route::get('/edit/{appointment}', ['as' => 'appointments.edit', 'uses' => 'AppointmentsController@edit']);
        Route::post('/edit/{appointment}', ['as' => 'appointments.edit', 'uses' => 'AppointmentsController@edit']);
        /*Route::get('/delete/{appointment}', ['as' => 'appointments.delete', 'uses' => 'AppointmentsController@delete']);
        Route::post('/delete/{appointment}', ['as' => 'appointments.delete', 'uses' => 'AppointmentsController@delete']);*/
        Route::post('/active/{appointment}', ['as' => 'appointments.active', 'uses' => 'AppointmentsController@active']);
        Route::post('/active/{appointment}/{status}', ['as' => 'appointments.active.appointment.status', 'uses' => 'AppointmentsController@active']);
        Route::post('/datasource', ['as' => 'appointments.datasource', 'uses' => 'AppointmentsController@datasource']);
        Route::post('/search', ['as' => 'appointments.search', 'uses' => 'AppointmentsController@search']);

    });

    /**
     * Routes of Technician
     */
    Route::group(['prefix' => 'technician'], function(){

        Route::get('/', ['as' => 'technician', 'uses' => 'TechnicianController@index']);
        Route::post('/', ['as' => 'technician', 'uses' => 'TechnicianController@index']);
        Route::get('/add', ['as' => 'technician.add', 'uses' => 'TechnicianController@add']);
        Route::post('/add', ['as' => 'technician.add', 'uses' => 'TechnicianController@add']);
        Route::get('/show/{requestedProcedure}', ['as' => 'technician.show', 'uses' => 'TechnicianController@show']);
        Route::post('/show/{requestedProcedure}', ['as' => 'technician.show', 'uses' => 'TechnicianController@show']);
        Route::get('/edit/{requestedProcedure}', ['as' => 'technician.edit', 'uses' => 'TechnicianController@edit']);
        Route::post('/edit/{requestedProcedure}', ['as' => 'technician.edit', 'uses' => 'TechnicianController@edit']);
        /*Route::get('/delete/{requestedProcedure}', ['as' => 'technician.delete', 'uses' => 'TechnicianController@delete']);
        Route::post('/delete/{requestedProcedure}', ['as' => 'technician.delete', 'uses' => 'TechnicianController@delete']);*/
        Route::post('/active/{requestedProcedure}', ['as' => 'technician.active', 'uses' => 'TechnicianController@active']);

    });

    /**
     * Routes of Radiologist
     */
    Route::group(['prefix' => 'radiologist'], function(){

        Route::post('/orders', ['as' => 'radiologist.orders', 'uses' => 'RadiologistController@orders']);
        Route::get('/{status}', ['as' => 'radiologist', 'uses' => 'RadiologistController@index']);
        Route::post('/{status}', ['as' => 'radiologist', 'uses' => 'RadiologistController@index']);
        Route::get('/add', ['as' => 'radiologist.add', 'uses' => 'RadiologistController@add']);
        Route::post('/add', ['as' => 'radiologist.add', 'uses' => 'RadiologistController@add']);
        Route::get('/show/{requestedProcedure}', ['as' => 'radiologist.show', 'uses' => 'RadiologistController@show']);//borrar
        Route::post('/show/{requestedProcedure}', ['as' => 'radiologist.show', 'uses' => 'RadiologistController@show']);//borrar
        Route::get('/edit/{requestedProcedure}', ['as' => 'radiologist.edit', 'uses' => 'RadiologistController@edit']);//borrar
        Route::post('/edit/{requestedProcedure}', ['as' => 'radiologist.edit', 'uses' => 'RadiologistController@edit']);//borrar
        /*Route::get('/delete/{requestedProcedure}', ['as' => 'radiologist.delete', 'uses' => 'RadiologistController@delete']);
        Route::post('/delete/{requestedProcedure}', ['as' => 'radiologist.delete', 'uses' => 'RadiologistController@delete']);*/
        Route::post('/active/{requestedProcedure}', ['as' => 'radiologist.active', 'uses' => 'RadiologistController@active']);
        Route::get('/datasource/{status}', ['as' => 'radiologist.datasource', 'uses' => 'RadiologistController@datasource']);
        Route::post('/datasource/{status}', ['as' => 'radiologist.datasource', 'uses' => 'RadiologistController@datasource']);
    });

    /**
     * Routes of PatientStates
     */

    Route::group(['prefix' => 'patientstates'], function(){

        Route::get('/', ['as' => 'patientStates', 'uses' => 'PatientStatesController@index']);
        Route::post('/', ['as' => 'patientStates', 'uses' => 'PatientStatesController@index']);
        Route::post('/roots', ['as' => 'patientStates.roots', 'uses' => 'PatientStatesController@roots']);
        Route::get('/add', ['as' => 'patientStates.add', 'uses' => 'PatientStatesController@add']);
        Route::post('/add', ['as' => 'patientStates.add', 'uses' => 'PatientStatesController@add']);
        Route::get('/show/{patientState}', ['as' => 'patientStates.show', 'uses' => 'PatientStatesController@show']);
        Route::post('/show/{patientState}', ['as' => 'patientStates.show', 'uses' => 'PatientStatesController@show']);
        Route::get('/edit/{patientState}', ['as' => 'patientStates.edit', 'uses' => 'PatientStatesController@edit']);
        Route::post('/edit/{patientState}', ['as' => 'patientStates.edit', 'uses' => 'PatientStatesController@edit']);
        /*Route::get('/delete/{patientState}',['as' => 'patientStates.delete', 'uses' => 'PatientStatesController@delete']);
        Route::post('/delete/{patientState}',['as' => 'patientStates.delete', 'uses' => 'PatientStatesController@delete']);*/
        Route::post('/active/{patientState}', ['as' => 'patientStates.active', 'uses' => 'PatientStatesController@active']);

    });

    /**
     * Routes of Addendums
     */
    Route::group(['prefix' => 'addendums'], function() {
        Route::get('/', ['as' => 'addendums', 'uses' => 'AddendumsController@index']);
        Route::post('/', ['as' => 'addendums', 'uses' => 'AddendumsController@index']);
        Route::get('/add/', ['as' => 'addendums.add', 'uses' => 'AddendumsController@add']);
        Route::post('/add/', ['as' => 'addendums.add', 'uses' => 'AddendumsController@add']);
        Route::get('/show/{requestedProcedure}', ['as' => 'addendums.show', 'uses' => 'AddendumsController@show']);
        Route::post('/show/{requestedProcedure}', ['as' => 'addendums.show', 'uses' => 'AddendumsController@show']);
        Route::get('/edit/{requestedProcedure}', ['as' => 'addendums.edit', 'uses' => 'AddendumsController@edit']);
        Route::post('/edit/{requestedProcedure}', ['as' => 'addendums.edit', 'uses' => 'AddendumsController@edit']);
        Route::post('/active/{requestedProcedure}', ['as' => 'addendums.active', 'uses' => 'AddendumsController@active']);

    });

    /**
     * Routes of Pre-Admission
     */
    Route::group(['prefix' => 'preadmission'], function(){

        Route::get('/', ['as' => 'preadmission', 'uses' => 'PreAdmissionController@index']);
        Route::post('/', ['as' => 'preadmission', 'uses' => 'PreAdmissionController@index']);

        Route::get('/add', ['as' => 'preadmission.add', 'uses' => 'PreAdmissionController@add']);
        Route::post('/add', ['as' => 'preadmission.add', 'uses' => 'PreAdmissionController@add']);

        Route::get('/show/{requestedProcedure}', ['as' => 'preadmission.show', 'uses' => 'PreAdmissionController@show']);
        Route::post('/show/{requestedProcedure}', ['as' => 'preadmission.show', 'uses' => 'PreAdmissionController@show']);

        Route::post('/active/{requestedProcedure}', ['as' => 'preadmission.active', 'uses' => 'PreAdmissionController@active']);
        Route::post('/write/{requestedProcedure}', ['as' => 'preadmission.write', 'uses' => 'PreAdmissionController@write']);

        Route::get('/edit/{requestedProcedure}/{serviceRequest}', ['as' => 'preadmission.edit', 'uses' => 'PreAdmissionController@edit']);
        Route::post('/edit/{requestedProcedure}/{serviceRequest}', ['as' => 'preadmission.edit', 'uses' => 'PreAdmissionController@edit']);

    });

    /**
     * Routes of Search
     */
    Route::group(['prefix' => 'search'], function(){

        Route::get('/', ['as' => 'search', 'uses' => 'SearchController@index']);
        Route::post('/', ['as' => 'search', 'uses' => 'SearchController@index']);

        Route::get('/add', ['as' => 'search.add', 'uses' => 'SearchController@add']);
        Route::post('/add', ['as' => 'search.add', 'uses' => 'SearchController@add']);

        Route::get('/show/{requestedProcedure}', ['as' => 'search.show', 'uses' => 'SearchController@show']);
        Route::post('/show/{requestedProcedure}', ['as' => 'search.show', 'uses' => 'SearchController@show']);

        Route::post('/active/{requestedProcedure}', ['as' => 'search.active', 'uses' => 'SearchController@active']);
        Route::post('/write/{requestedProcedure}', ['as' => 'search.write', 'uses' => 'SearchController@write']);

        Route::get('/edit/{requestedProcedure}', ['as' => 'search.edit', 'uses' => 'SearchController@edit']);
        Route::post('/edit/{requestedProcedure}/', ['as' => 'search.edit', 'uses' => 'SearchController@edit']);

    });

    /**
     * Routes of Categories
     */
    Route::group(['prefix' => 'categories'], function(){

        Route::get('/', ['as' => 'categories', 'uses' => 'CategoriesController@index']);
        Route::post('/', ['as' => 'categories', 'uses' => 'CategoriesController@index']);
        Route::get('/add', ['as' => 'categories.add', 'uses' => 'CategoriesController@add']);
        Route::post('/add', ['as' => 'categories.add', 'uses' => 'CategoriesController@add']);
        Route::get('/show/{category}', ['as' => 'categories.show', 'uses' => 'CategoriesController@show']);
        Route::post('/show/{category}', ['as' => 'categories.show', 'uses' => 'CategoriesController@show']);
        Route::get('/edit/{category}', ['as' => 'categories.edit', 'uses' => 'CategoriesController@edit']);
        Route::post('/edit/{category}', ['as' => 'categories.edit', 'uses' => 'CategoriesController@edit']);
        /*Route::get('/delete/{category}', ['as' => 'categories.delete', 'uses' => 'CategoriesController@delete']);
        Route::post('/delete/{category}', ['as' => 'categories.delete', 'uses' => 'CategoriesController@delete']);*/
        Route::post('/active/{category}', ['as' => 'categories.active', 'uses' => 'CategoriesController@active']);

    });

    /**
     * Routes of SubCategories
     */
    Route::group(['prefix' => 'subcategories'], function(){

        Route::get('/', ['as' => 'subcategories', 'uses' => 'SubCategoriesController@index']);
        Route::post('/', ['as' => 'subcategories', 'uses' => 'SubCategoriesController@index']);
        Route::get('/add', ['as' => 'subcategories.add', 'uses' => 'SubCategoriesController@add']);
        Route::post('/add', ['as' => 'subcategories.add', 'uses' => 'SubCategoriesController@add']);
        Route::get('/show/{subcategory}', ['as' => 'subcategories.show', 'uses' => 'SubCategoriesController@show']);
        Route::post('/show/{subcategory}', ['as' => 'subcategories.show', 'uses' => 'SubCategoriesController@show']);
        Route::get('/edit/{subcategory}', ['as' => 'subcategories.edit', 'uses' => 'SubCategoriesController@edit']);
        Route::post('/edit/{subcategory}', ['as' => 'subcategories.edit', 'uses' => 'SubCategoriesController@edit']);
        /*Route::get('/delete/{subcategory}', ['as' => 'subcategories.delete', 'uses' => 'SubCategoriesController@delete']);
        Route::post('/delete/{subcategory}', ['as' => 'subcategories.delete', 'uses' => 'SubCategoriesController@delete']);*/
        Route::post('/active/{subcategory}', ['as' => 'subcategories.active', 'uses' => 'SubCategoriesController@active']);

    });

    /**
     * Routes of Transcriber
     */
    Route::group(['prefix' => 'transcriber'], function(){

        Route::get('/', ['as' => 'transcriber', 'uses' => 'TranscriberController@index']);
        Route::post('/', ['as' => 'transcriber', 'uses' => 'TranscriberController@index']);
        Route::get('/show/{requestedProcedure}', ['as' => 'transcriber.show', 'uses' => 'TranscriberController@show']);
        Route::post('/show/{requestedProcedure}', ['as' => 'transcriber.show', 'uses' => 'TranscriberController@show']);
        //Route::get('/edit/{requestedProcedure}', ['as' => 'transcriber.edit', 'uses' => 'TranscriberController@edit']);
        //Route::post('/edit/{requestedProcedure}', ['as' => 'transcriber.edit', 'uses' => 'TranscriberController@edit']);
    });

    /**
     * Routes of Results
     */
    Route::group(['prefix' => 'results'], function(){

        Route::get('/', ['as' => 'results', 'uses' => 'ResultsController@index']);
        Route::post('/', ['as' => 'results', 'uses' => 'ResultsController@index']);
        Route::get('/show/{requestedProcedure}', ['as' => 'results.show', 'uses' => 'ResultsController@show']);
        Route::post('/show/{requestedProcedure}', ['as' => 'results.show', 'uses' => 'ResultsController@show']);
        Route::get('/edit/{requestedProcedure}', ['as' => 'results.edit', 'uses' => 'ResultsController@edit']);
        Route::post('/edit/{requestedProcedure}', ['as' => 'results.edit', 'uses' => 'ResultsController@edit']);

    });
    /**
     * 
     * Routes polls
     * 
     */

    Route::group(['prefix' => 'polls'], function(){

        Route::post('/', ['as' => 'polls', 'uses' => 'ResultsController@getPollsData']);
    });

    /**
     * Routes of Delivers
     */
    Route::group(['prefix' => 'delivers'], function(){

        Route::get('/', ['as' => 'delivers', 'uses' => 'DeliversController@index']);
        Route::post('/', ['as' => 'delivers', 'uses' => 'DeliversController@index']);
        Route::get('/add', ['as' => 'delivers.add', 'uses' => 'DeliversController@add']);
        Route::post('/add', ['as' => 'delivers.add', 'uses' => 'DeliversController@add']);

        
    });

    /**
     * Routes of Responsables
     */
    Route::group(['prefix' => 'responsables'], function(){
        Route::get('/', ['as' => 'responsables', 'uses' => 'ResponsablesController@index']);
        Route::post('/', ['as' => 'responsables', 'uses' => 'ResponsablesController@index']);
    });

    /**
     * Routes of Sexes
     */
    Route::group(['prefix' => 'sexes'], function(){
        Route::get('/', ['as' => 'sexes', 'uses' => 'SexesController@index']);
        Route::post('/', ['as' => 'sexes', 'uses' => 'SexesController@index']);
    });

    /**
     * Routes of RequestStatuses
     */
    Route::group(['prefix' => 'requeststatuses'], function(){
        Route::get('/', ['as' => 'requeststatuses', 'uses' => 'RequestStatusesController@index']);
        Route::post('/', ['as' => 'requeststatuses', 'uses' => 'RequestStatusesController@index']);
    });

    /**
     * Routes of RequestedProcedureStatus
     */
    Route::group(['prefix' => 'requestedprocedurestatus'], function(){
        Route::get('/', ['as' => 'requestedprocedurestatus', 'uses' => 'RequestedProcedureStatusController@index']);
        Route::post('/', ['as' => 'requestedprocedurestatus', 'uses' => 'RequestedProcedureStatusController@index']);
    });

    /**
     * Routes of SmokingStatuses
     */
    Route::group(['prefix' => 'smokingstatuses'], function(){
        Route::get('/', ['as' => 'smokingstatuses', 'uses' => 'SmokingStatusesController@index']);
        Route::post('/', ['as' => 'smokingstatuses', 'uses' => 'SmokingStatusesController@index']);
    });

    /**
     * Routes of PregnancyStatuses
     */
    Route::group(['prefix' => 'pregnancystatuses'], function(){
        Route::get('/', ['as' => 'pregnancystatuses', 'uses' => 'PregnancyStatusesController@index']);
        Route::post('/', ['as' => 'pregnancystatuses', 'uses' => 'PregnancyStatusesController@index']);
    });

    /**
     * Routes of Answers
     */
    Route::group(['prefix' => 'answers'], function(){
        Route::get('/', ['as' => 'answers', 'uses' => 'AnswersController@index']);
        Route::post('/', ['as' => 'answers', 'uses' => 'AnswersController@index']);
    });

    /**
     * Routes of AppointmentStatuses
     */
    Route::group(['prefix' => 'appointmentstatuses'], function(){
        Route::get('/', ['as' => 'appointmentstatuses', 'uses' => 'AppointmentStatusesController@index']);
        Route::post('/', ['as' => 'appointmentstatuses', 'uses' => 'AppointmentStatusesController@index']);
    });

    /**
     * Routes of BiRads
     */
    Route::group(['prefix' => 'birads'], function(){
        Route::get('/', ['as' => 'birads', 'uses' => 'BiRadsController@index']);
        Route::post('/', ['as' => 'birads', 'uses' => 'BiRadsController@index']);
        Route::get('/show/{birad}', ['as' => 'birads.show', 'uses' => 'BiRadsController@show']);
        Route::post('/show/{birad}', ['as' => 'birads.show', 'uses' => 'BiRadsController@show']);
    });

    /** 
     * Routes of Reports
     */
    
    Route::group(['prefix' => 'reports'], function(){

        Route::post('/{name}', ['as' => 'reports', 'uses' => 'ReportsController@index']);
    });


    /**
     * Routes of MedicalVisitors
     */

    Route::group(['prefix' => 'medicalvisitors'], function(){
        Route::get('/', ['as' => 'medicalvisitors', 'uses' => 'MedicalVisitorsController@index']);
        Route::post('/', ['as' => 'medicalvisitors', 'uses' => 'MedicalVisitorsController@index']);
        Route::get('/add', ['as' => 'medicalvisitors.add', 'uses' => 'MedicalVisitorsController@add']);
        Route::post('/add', ['as' => 'medicalvisitors.add', 'uses' => 'MedicalVisitorsController@add']);
        Route::get('/show/{medicalvisitor}', ['as' => 'medicalvisitors.show', 'uses' => 'MedicalVisitorsController@show']);
        Route::post('/show/{medicalvisitor}', ['as' => 'medicalvisitors.show', 'uses' => 'MedicalVisitorsController@show']);
        Route::get('/edit/{medicalvisitor}', ['as' => 'medicalvisitors.edit', 'uses' => 'MedicalVisitorsController@edit']);
        Route::post('/edit/{medicalvisitor}', ['as' => 'medicalvisitors.edit', 'uses' => 'MedicalVisitorsController@edit']);
        Route::post('/active/{medicalvisitor}', ['as' => 'medicalvisitors.active', 'uses' => 'MedicalVisitorsController@active']);
    });

});
