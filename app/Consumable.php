<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consumable extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'description', 'unit_id', 'active',
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Muchos Consumables tienen muchos Steps.
     */
    public function steps()
    {
        return $this->belongsToMany(Step::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un consumable tiene un Unit.
     */
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active()
    {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}
