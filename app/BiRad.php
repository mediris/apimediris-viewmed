<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BiRad extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'id',
        'description',
        'frecuency'
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un BiRad tiene muchos RequestedProcedures.
     */
    public function requestedProcedures()
    {
        return $this->hasMany(RequestedProcedure::class);
    }
}
