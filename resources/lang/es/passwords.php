<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Contrase&ntilde;a debe tener al menos seis caracteres e igual a la confirmaci&oacute;n.',
    'reset' => 'Tu contrase&ntilde;a ha sido restablecida',
    'sent' => 'Hemos enviado por correo electr&oacute;nico el enlace para restablecer la contrase&ntilde;a.',
    'token' => 'Este token de restablecimiento de contrase&ntilde;a no es v&aacute;lido.',
    'user' => "No encontramos un usuario con ese correo electr&oacute;nico.",

];
