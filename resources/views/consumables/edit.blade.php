@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('messages.consumable') }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('consumables.edit', [$consumable]) }}">
                            {!! csrf_field() !!}


                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="description" value="{{ $consumable->description }}">

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('units') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Units</label>

                                <div class="col-md-6">
                                    <input type="number" step="1" class="form-control" name="units" value="{{ $consumable->units }}">

                                    @if ($errors->has('units'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('units') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('institutions') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">institutions</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="institutions[]" multiple>
                                        @foreach($institutions as $institution)
                                            @if($consumable->haveInstitution($institution->id))
                                                <option value="{{ $institution->id }}" selected>{{ $institution->name }}</option>
                                            @else
                                                <option value="{{ $institution->id }}">{{ $institution->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('institutions'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('institutions') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ ucfirst(trans('messages.edit')) }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
