@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit a Room</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('rooms.edit', [$room]) }}">
                            {!! csrf_field() !!}


                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ $room->name }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" name="description">{{ $room->description }}</textarea>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('administrative_ID') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Administrative ID</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="administrative_ID" value="{{ $room->administrative_ID }}">

                                    @if ($errors->has('administrative_ID'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('administrative_ID') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Is Active?</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="active" value="1" {{ $room->isActive() }}>

                                    @if ($errors->has('active'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('active') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('division_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ ucfirst(trans('messages.division')) }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="division_id">
                                        <option value="">Select</option>
                                        @foreach($divisions as $division)
                                            @if($division->id == $room->division_id)
                                                <option value="{{ $division->id }}" selected>{{ $division->name }}</option>
                                            @else
                                                <option value="{{ $division->id }}">{{ $division->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('division_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('division_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>




                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
