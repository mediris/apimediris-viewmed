@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ ucfirst(trans('messages.add')) }} {{ ucfirst(trans('messages.patient-type')) }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('patientTypes.edit', [$patientType]) }}">
                            {!! csrf_field() !!}


                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="description" value="{{ $patientType->description }}">

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('administrative_ID') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Administrative ID</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="administrative_ID" value="{{ $patientType->administrative_id }}">

                                    @if ($errors->has('administrative_ID'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('administrative_ID') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('priority') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Priority</label>

                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="priority" value="{{ $patientType->priority }}">

                                    @if ($errors->has('priority'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('priority') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Is Active?</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="active" value="1" {{ $patientType->active == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('active'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('active') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Icon</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="icon" value="{{ $patientType->icon }}">

                                    @if ($errors->has('icon'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('icon') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('admin_aprob') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Admin aprobbation?</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="admin_aprob" value="1" {{ $patientType->admin_aprob == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('admin_aprob'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('admin_aprob') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('sms_send') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">sms_send?</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="sms_send" value="1" {{ $patientType->sms_send == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('sms_send'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sms_send') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email_patient') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">email_patient?</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="email_patient" value="1" {{ $patientType->email_patient == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('email_patient'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email_patient') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email_refeer') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">email_refeer?</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="email_refeer" value="1" {{ $patientType->email_refeer == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('email_refeer'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email_refeer') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ trans('messages.edit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
