@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>Modalities</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif


        <a href="{{ route('modalities.add') }}">Add new +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
            @foreach($modalities as $modality)
                <tr>
                    <td>{{ $modality->id }}</td>
                    <td>{{ $modality->name }}</td>
                    <td><a href="{{ route('modalities.show', [$modality]) }}">Show</a> | <a href="{{ route('modalities.edit', [$modality]) }}">Edit</a> | <a href="{{ route('modalities.delete', [$modality]) }}" onclick="return confirm('{{ trans("messages.delete") }}')">Delete</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection