<!-- JavaScripts -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
 {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}


<!-- BEGIN MANDATORY SCRIPTS -->
<script src="{{ '/plugins/jquery-migrate-1.2.1.js' }}"></script>
<script src="{{ '/plugins/jquery-ui/jquery-ui-1.10.4.min.js' }}"></script>
<script src="{{ '/plugins/jquery-mobile/jquery.mobile-1.4.2.js' }}"></script>
<script src="{{ '/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js' }}"></script>
<script src="{{ '/plugins/jquery.cookie.min.js' }}"></script>
<script src="{{ '/js/application.js' }}"></script>
<!-- END MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ '/plugins/backstretch/backstretch.min.js' }}"></script>
<script src="{{ '/plugins/bootstrap-loading/lada.min.js' }}"></script>


<script src="{{ '/plugins/charts-flot/jquery.flot.js' }}"></script>
<script src="{{ '/plugins/charts-flot/jquery.flot.animator.min.js' }}"></script>
<script src="{{ '/plugins/charts-flot/jquery.flot.resize.js' }}"></script>
<script src="{{ '/plugins/charts-flot/jquery.flot.time.min.js' }}"></script>
<script src="{{ '/plugins/charts-morris/raphael.min.js' }}"></script>
<script src="{{ '/plugins/charts-morris/morris.min.js' }}"></script>
<script src="{{ '/plugins/charts-d3/d3.v3.js' }}" ></script>
<script src="{{ '/plugins/charts-d3/nv.d3.js' }}" ></script>
<script src="{{ '/js/charts.js'}}"></script>


<script src="{{ '/js/account.js' }}"></script>
<script src="{{ '/plugins/revolution/plugins.min.js' }}"></script>
<script src="{{ '/plugins/revolution/jquery.revolution.min.js' }}"></script>
<script src="{{ '/js/logoscorp.js' }}"></script>
<!-- END PAGE LEVEL SCRIPTS -->






