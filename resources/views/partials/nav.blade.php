@if (!Auth::guest())
    <nav id="sidebar" >
        <div id="main-menu" >
            <ul class="sidebar-nav align-center">
                <li class="{{ (Request::segment(3) == null)? 'current active' : '' }}">
                    <a href="/"><i class="ico icon-principal"></i><span class="sidebar-text">Principal</span><span class="glyphicon glyphicon-triangle-left arrow-white" id="principal-arrow-white"></span></a>
                </li>
                <li>
                    <a href="citas.html" target="_self"><i class="ico icon-citas"></i><span class="sidebar-text">Citas</span><span class="glyphicon glyphicon-triangle-left arrow-white" id="citas-arrow-white"></span></a>
                </li>
                <li>
                    <a href="recepcion.html" target="_self"><i class="ico icon-recepcion"></i><span class="sidebar-text">Recepción</span><span class="glyphicon glyphicon-triangle-left arrow-white" id="recepcion-arrow-white"></span></a>
                </li>
                <li>
                    <a href="#" target="_self"><i class="ico icon-preingreso"></i><span class="sidebar-text">Pre-ingreso</span><span class="glyphicon glyphicon-triangle-left arrow-white" id="preingreso-arrow-white"></span></a>
                </li>
                <li>
                    <a href="#" target="_self"><i class="ico icon-entrevista"></i><span class="sidebar-text">Entrevista</span><span class="glyphicon glyphicon-triangle-left arrow-white" id="entrevista-arrow-white"></span></a>
                </li>
                <li>
                    <a href="tecnico.html" target="_self"><i class="ico icon-tecnico"></i><span class="sidebar-text">Técnico</span><span class="glyphicon glyphicon-triangle-left arrow-white" id="tecnico-arrow-white"></span></a>
                </li>
                <li>
                    <a href="#"><i class="ico icon-radiologo "></i><span class="sidebar-text ">Radiólogo <span class="arrow arrow glyphicon glyphicon-triangle-right" id="arrow-rad"></span></span><span class="glyphicon glyphicon-triangle-left arrow-white" id="radiologo-arrow-white"></span></a>
                    <ul class="submenu collapse">
                        <li>
                            <a href="transcripcion_black.html"><span class="sidebar-text">Ordenes por dictar</span></a>
                        </li>
                        <li>
                            <a href="#"><span class="sidebar-text">Ordenes por aprobar</span></a>
                        </li>
                        <li>
                            <a href="#"><span class="sidebar-text">Addendum</span></a>
                        </li>
                        <li>
                            <a href="#"><span class="sidebar-text">Ordenes dictadas</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="transcripcion.html" target="_self"><i class="ico icon-transcripcion"></i><span class="sidebar-text">Transcripción</span><span class="glyphicon glyphicon-triangle-left arrow-white" id="transcripcion-arrow-white"></span></a>
                </li>
                <li>
                    <a href="resultados.html" target="_self"><i class="ico icon-resultados"></i><span class="sidebar-text">Resultados</span><span class="glyphicon glyphicon-triangle-left arrow-white" id="resultados-arrow-white"></span></a>
                </li>
                <li>
                    <a href="busqueda.html" target="_self"><i class="ico icon-busqueda"></i><span class="sidebar-text">Busqueda</span><span class="glyphicon glyphicon-triangle-left arrow-white" id="busqueda-arrow-white"></span></a>
                </li>
                <li>
                    <a href="#" target="_self"><i class="ico icon-pacientes"></i><span class="sidebar-text">Paciente</span><span class="glyphicon glyphicon-triangle-left arrow-white" id="pacientes-arrow-white"></span></a>
                </li>
                <li>
                    <a href="#" target="_self"><i class="ico icon-reportes"></i><span class="sidebar-text">Reportes</span><span class="glyphicon glyphicon-triangle-left arrow-white" id="reportes-arrow-white"></span></a>
                </li>
                <!--<li>
                    <a href="#" target="_self"><i class="ico icon-administracion"></i><span class="sidebar-text">Administración</span></a>
                </li>-->
                <li class="{{(Request::segment(3) == 'sections' || Request::segment(3) == 'actions' || Request::segment(3) == 'roles' || Request::segment(3) == 'users' || Request::segment(3) == 'institutions' || Request::segment(3) == 'division' || Request::segment(3) == 'rooms' || Request::segment(3) == 'modalities' || Request::segment(3) == 'equipment' || Request::segment(3) == 'referrings' || Request::segment(3) == 'configurations' || Request::segment(3) == 'patientTypes' || Request::segment(3) == 'sources' || Request::segment(3) == 'procedures' || Request::segment(3) == 'templates' || Request::segment(3) == 'consumables')? 'current active' : '' }}">
                    <a href="#"><i class="ico icon-administracion"></i><span class="sidebar-text ">Administraci&oacute;n <span class="arrow arrow glyphicon glyphicon-triangle-right {{(Request::segment(3) == 'sections' || Request::segment(3) == 'actions' || Request::segment(3) == 'roles' || Request::segment(3) == 'users' || Request::segment(3) == 'institutions' || Request::segment(3) == 'division' || Request::segment(3) == 'rooms' || Request::segment(3) == 'modalities' || Request::segment(3) == 'equipment' || Request::segment(3) == 'referrings' || Request::segment(3) == 'configurations' || Request::segment(3) == 'patientTypes' || Request::segment(3) == 'sources' || Request::segment(3) == 'procedures' || Request::segment(3) == 'templates' || Request::segment(3) == 'consumables')? 'arrow-down' : '' }}" id="arrow-adm"></span></span><span class="glyphicon glyphicon-triangle-left arrow-white" id="administracion-arrow-white"></span></a>
                    <ul class="submenu collapse">
                        <li><a href="{{ route('sections') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.sections')) }}</span></a></li>
                        <li><a href="{{ route('actions') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.actions')) }}</span></a></li>
                        <li><a href="{{ route('roles') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.roles')) }}</span></a></li>
                        <li><a href="{{ route('users') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.users')) }}</span></a></li>
                        <li><a href="{{ route('institutions') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.institutions')) }}</span></a></li>
                        <li><a href="{{ route('divisions') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.divisions')) }}</span></a></li>
                        <li><a href="{{ route('rooms') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.rooms')) }}</span></a></li>
                        <li><a href="{{ route('modalities') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.modalities')) }}</span></a></li>
                        <li><a href="{{ route('equipment') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.equipment')) }}</span></a></li>
                        <li><a href="{{ route('referrings') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.referrings')) }}</span></a></li>
                        <li><a href="{{ route('configurations') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.configurations')) }}</span></a></li>
                        <li><a href="{{ route('patientTypes') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.patient-types')) }}</span></a></li>
                        <li><a href="{{ route('sources') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.sources')) }}</span></a></li>
                        <li><a href="{{ route('procedures') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.procedures')) }}</span></a></li>
                        <li><a href="{{ route('templates') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.templates')) }}</span></a></li>
                        <li><a href="{{ route('consumables') }}"><span class="sidebar-text">{{ ucfirst(trans('messages.consumables')) }}</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
@endif