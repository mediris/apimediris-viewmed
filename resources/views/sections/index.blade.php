@extends('layouts.app')

@section('title','SECTIONS')

@section('content')
    <div class="container">
        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif
        <div class="search">
            <form method="post" action="{{ url('/sections') }}">
                {!! csrf_field() !!}
                <fieldset>
                    <div class="row">
                        <div class="col-md-3">
                            <legend>B&uacute;squeda b&aacute;sica</legend>
                        </div>
                     </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div>
                                <label for='inpt_search'>Buscar</label>
                            </div>
                            <div>
                                <input type="text" name="search" id="inpt_search" class="input-field form-control user btn-style"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div>
                                <label for="range">Per&iacute;odo</label>
                            </div>
                            <div>
                                <select id="range" class="form-control selectpicker" data-live-search="true">
                                    <option value="Todos" selected="selected">Todos</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                    <option value="">Option 3</option>
                                    <option value="">Option 4</option>
                                    <option value="">Option 5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div>
                                <label for="modal">Modalidad</label>
                            </div>
                            <div>
                                <select id="modal" class="form-control selectpicker" data-live-search="true">
                                    <option value="Todos" selected="selected">Cualquiera</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                    <option value="">Option 3</option>
                                    <option value="">Option 4</option>
                                    <option value="">Option 5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <br>
                            <div>
                                <button id="btn-basic-search" class="btn btn-form ladda-button" data-style="expand-left" type="submit"><span class="ladda-label">Buscar</span></button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
            <form method="post" action="{{ url('/sections') }}">
                {!! csrf_field() !!}
                <fieldset id="fls-avz-search">
                    <div class="row">
                        <div class="col-md-3">
                            <legend><a herf="#" id="b_avanzada_btn">B&uacute;squeda avanzada</a></legend>
                        </div>
                    </div>
                    <div class="row" id="b_avanzada">
                        <div class="col-md-3">
                            <div>
                                <label for='inpt_search'>Procedimiento</label>
                            </div>
                            <div>
                                <input type="text" name="search" id="inpt_search" class="input-field form-control user btn-style"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div>
                                <label for="range">M&eacute;dico asignado</label>
                            </div>
                            <div>
                                <select id="range" class="form-control selectpicker" data-live-search="true">
                                    <option value="Todos" selected="selected">Todos</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                    <option value="">Option 3</option>
                                    <option value="">Option 4</option>
                                    <option value="">Option 5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div>
                                <label for="modal">Dictado por</label>
                            </div>
                            <div>
                                <select id="modal" class="form-control selectpicker" data-live-search="true">
                                    <option value="Todos" selected="selected">Cualquiera</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                    <option value="">Option 3</option>
                                    <option value="">Option 4</option>
                                    <option value="">Option 5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <br>
                            <div>
                                <button id="btn-avz-search" class="btn btn-form ladda-button" data-style="expand-left" type="submit"><span class="ladda-label">Buscar</span></button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="title">
            <h1>{{ ucfirst(trans('messages.sections')) }}</h1>
        </div>
       <!-- <a href="{{ route('sections.add') }}">{{ ucfirst(trans('messages.add')) }} +</a> -->
        <div id="table-container">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sections as $section)
                        <tr>
                            <td>{{ $section->id }}</td>
                            <td>{{ $section->name }}</td>
                            <td><a href="{{ route('sections.show', [$section]) }}"><img src="/images/icon_ver_off.png" alt="{{ ucfirst(trans('messages.show')) }}" title="{{ ucfirst(trans('messages.show')) }}" width="26"></a><a href="{{ route('sections.edit', [$section]) }}"><img src="/images/icon_edit_off.png" alt="{{ ucfirst(trans('messages.edit')) }}" title="{{ ucfirst(trans('messages.edit')) }}" width="26"></a><a href="{{ route('sections.delete', [$section]) }}" onclick="return confirm('{{ trans("messages.delete") }}')"><img src="/images/icon_eliminar_off.png" alt="{{ ucfirst(trans('messages.purge')) }}" title="{{ ucfirst(trans('messages.purge')) }}" width="26"></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection